import React, { FC, useEffect } from "react";
import { connect } from "react-redux";
import { ThemeProvider, createMuiTheme, CssBaseline } from "@material-ui/core";
import { green, amber } from "@material-ui/core/colors";
import App from "../App";
import { State } from "../../utils/types";
import { ThemeVariant, LSKey } from "../../utils/enums";
import { putLSKey } from "../../utils/localstorage";

const lightTheme = createMuiTheme({
  palette: {
    primary: {
      main: green[800],
      dark: green[900],
      light: green[500]
    },
    secondary: {
      main: amber[400],
      dark: amber[600],
      light: amber[800]
    }
  }
});

const darkTheme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: green[800],
      dark: green[900],
      light: green[500]
    },
    secondary: {
      main: amber[400],
      dark: amber[600],
      light: amber[800]
    }
  }
});

const getThemeByVariant = (themeVariant: ThemeVariant) => {
  if (themeVariant === ThemeVariant.DARK) {
    return darkTheme;
  } else if (themeVariant === ThemeVariant.LIGHT) {
    return lightTheme;
  } else {
    return lightTheme;
  }
};

type ThemedAppProps = {
  themeVariant: ThemeVariant;
};

const ThemedApp: FC<ThemedAppProps> = ({ themeVariant }) => {
  useEffect(() => {
    putLSKey(LSKey.THEME_VARIANT, themeVariant);
  }, [themeVariant]);

  const theme = getThemeByVariant(themeVariant);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  );
};

export default connect((state: State) => ({
  themeVariant: state.common.theme
}))(ThemedApp);
