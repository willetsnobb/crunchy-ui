import { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { useMediaQuery, withTheme, WithTheme } from "@material-ui/core";

import {
  setPersistDrawer,
  setDrawerOpen,
  setUseTables
} from "../../actions/commonActions";
import { State } from "../../utils/types";

const connector = connect(
  (state: State) => ({
    persistDrawer: state.common.persistDrawer,
    useTables: state.common.useTables
  }),
  { setPersistDrawer, setDrawerOpen, setUseTables }
);

type WidthObserverProps = WithTheme & ConnectedProps<typeof connector>;

const WidthObserver: FC<WidthObserverProps> = ({
  theme,
  useTables,
  persistDrawer,
  setDrawerOpen,
  setUseTables,
  setPersistDrawer
}) => {
  const shouldPersistDrawer = useMediaQuery(theme.breakpoints.up("lg")); // xs, sm, md, lg, xl
  const shouldUseTables = useMediaQuery(theme.breakpoints.up("md")); // xs, sm, md, lg, xl

  if (useTables !== shouldUseTables && !!setUseTables) {
    setUseTables(shouldUseTables);
  }

  if (
    persistDrawer !== shouldPersistDrawer &&
    !!setPersistDrawer &&
    !!setDrawerOpen
  ) {
    setPersistDrawer(shouldPersistDrawer);
    setDrawerOpen(shouldPersistDrawer); // open drawer if resize was from smaller window and backwards
  }

  return null;
};

export default withTheme(connector(WidthObserver));
