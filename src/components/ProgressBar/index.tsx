import React, { FC } from "react";
import { LinearProgress as MuiLinearProgress } from "@material-ui/core";
import { LinearProgressProps as MuiLinearProgressProps } from "@material-ui/core/LinearProgress";

interface ProgressBarProps extends MuiLinearProgressProps {}

const ProgressBar: FC<ProgressBarProps> = props => (
  <MuiLinearProgress {...props} />
);

export default ProgressBar;
