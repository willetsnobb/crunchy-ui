import React, { FC, HTMLAttributes, ReactNode } from "react";
import { Link } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import { useTranslation } from "react-i18next";
import {
  AppBar,
  Toolbar,
  IconButton,
  Icon,
  Typography,
  makeStyles,
  Theme,
  Button,
  Divider
} from "@material-ui/core";
import { setDrawerOpen } from "../../actions/commonActions";
import paths from "../../paths/paths";
import ProgressBar from "../ProgressBar";
import { State } from "../../utils/types";
import HeaderSetter from "../HeaderSetter";
import PathObserver from "../PathObserver";
import DrawerContent from "../DrawerContent";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    background: theme.palette.background.default
  },
  belowAppbarWrapper: {
    display: "flex",
    flexDirection: "row",
    flex: 1
  },
  divider: {
    width: 1,
    background: theme.palette.divider
  },
  pageContent: {
    flex: 1,
    minWidth: 0 // Firefox fails to resize it otherwise
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1,
    color: theme.palette.common.white
  },
  titleLink: {
    textDecoration: "none",
    color: theme.palette.common.white
  },
  list: {
    width: 250
  },
  footerDivider: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3)
  }
}));

const connector = connect(
  (state: State) => ({
    userInfo: state.login.user,
    header: state.common.header,
    drawerOpen: state.common.drawerOpen,
    persistDrawer: state.common.persistDrawer,
    loggedIn: !!state.login.user
  }),
  { setDrawerOpen }
);

type ComponentProps = {
  requestInProgress?: boolean;
  topAppBarIcons?: ReactNode;
  footer?: ReactNode;
};
type ReduxProps = ConnectedProps<typeof connector>;
type PageProps = ReduxProps & ComponentProps & HTMLAttributes<HTMLElement>;

const Page: FC<PageProps> = ({
  children,
  userInfo,
  header,
  topAppBarIcons,
  requestInProgress = false,
  drawerOpen,
  setDrawerOpen,
  persistDrawer,
  loggedIn,
  footer,
  ...props
}) => {
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <HeaderSetter />
      <PathObserver />

      <AppBar position="static">
        <Toolbar>
          {loggedIn && (
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
              onClick={() => setDrawerOpen(!drawerOpen)}
            >
              <Icon>menu</Icon>
            </IconButton>
          )}
          <Typography variant="h6" component="h1" className={classes.title}>
            {!header && (
              <Link className={classes.titleLink} to={paths.Main.toPath()}>
                <span role="img" aria-label="Coockie-image">
                  🍪
                </span>
                <span role="img" aria-label="discette-image">
                  💾
                </span>
                <span> Crunchy</span>
              </Link>
            )}
            {header}
          </Typography>

          {!userInfo && (
            <>
              <Link
                className="header-link-btn-end"
                style={{ color: "#fff", textDecoration: "none" }}
                to={paths.Register.toPath()}
              >
                <Button color="inherit">{t("register")}</Button>
              </Link>
              <Link
                className="header-link-btn-end"
                style={{ color: "#fff", textDecoration: "none" }}
                to={paths.Login.toPath()}
              >
                <Button color="inherit">{t("log_in")}</Button>
              </Link>
            </>
          )}

          {!!userInfo && !!topAppBarIcons && <>{topAppBarIcons}</>}
        </Toolbar>
      </AppBar>

      {requestInProgress && <ProgressBar />}
      {!requestInProgress && <div style={{ paddingTop: 4 }}> </div>}
      <div className={classes.belowAppbarWrapper}>
        {persistDrawer && loggedIn && drawerOpen && (
          <>
            <DrawerContent />
            <div className={classes.divider} />
          </>
        )}
        <main
          {...props}
          className={`${props.className || ""} ${classes.pageContent}`.trim()}
        >
          {children}

          {footer && (
            <>
              <Divider className={classes.footerDivider} />

              <footer>{footer}</footer>
            </>
          )}
        </main>
      </div>
    </div>
  );
};

export default connector(Page);
