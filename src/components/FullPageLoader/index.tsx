import React, { FC } from "react";
import { CircularProgress, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  wrapper: { display: "flex", height: "100%" },
  loader: {
    margin: "auto"
  }
}));

const FullPageLoader: FC<{}> = () => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <CircularProgress className={classes.loader} color="secondary" />
    </div>
  );
};

export default FullPageLoader;
