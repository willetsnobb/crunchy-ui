import React, { FC } from "react";
import { Card, makeStyles, Theme, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
  card: {
    background: theme.palette.primary.main,
    color: theme.palette.common.white,
    padding: theme.spacing(3),
    boxSizing: "border-box",
    width: "100%"
  }
}));

interface InfoCardProps {
  title: string | null;
  value: string | number | null;
}

const InfoCard: FC<InfoCardProps> = ({ title, value }) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <Typography variant="h6" gutterBottom>
        {title}
      </Typography>
      <Typography>{value}</Typography>
    </Card>
  );
};

export default InfoCard;
