import React, { useEffect, useState, FC, ComponentType } from "react";
import { connect, ConnectedProps } from "react-redux";
import {
  Route,
  withRouter,
  RouteProps,
  RouteComponentProps
} from "react-router-dom";
import { logout, TokensType, setTokens } from "../../services/authService";
import FullScreenLoader from "../FullScreenLoader";
import { State } from "../../utils/types";
import { getUnixSecTime } from "../../utils/time";
import { getLSKey } from "../../utils/localstorage";
import { LSKey } from "../../utils/enums";
import { refreshToken } from "../../actions/authActions";

const connector = connect((state: State) => ({
  token: state.login.token,
  tokenExpiresAt: state.login.tokenExpiresAt
}));

type OwnProps = {
  component: ComponentType;
};

type PrivateRouteProps = OwnProps &
  ConnectedProps<typeof connector> &
  RouteProps &
  RouteComponentProps;

const PrivateRoute: FC<PrivateRouteProps> = ({
  token,
  tokenExpiresAt,
  component: Component,
  location: { pathname },
  ...rest
}) => {
  const [lastCheckedPathname, setLastCheckedPathname] = useState("");

  useEffect(() => {
    const processTokensBeforePageLoads = async (): Promise<boolean> => {
      const unixSecTime = getUnixSecTime();

      const isTokenExpired: boolean = unixSecTime >= tokenExpiresAt;

      if (!isTokenExpired) {
        return true;
      }

      const refresh_token = getLSKey(LSKey.REFRESH_TOKEN, null);

      if (!refresh_token) {
        return false;
      }

      const refreshTokwenExpiresAt = getLSKey(
        LSKey.REFRESH_TOKEN_EXPIRES_AT,
        0
      );
      const refreshTokwenIsExpired = unixSecTime >= refreshTokwenExpiresAt;

      if (refreshTokwenIsExpired) {
        return false;
      }

      return refreshToken().then((response: any) => {
        const tokens: TokensType =
          response && response.data && response.data.data;

        setTokens(tokens);

        return true;
      });
    };

    const handleFail = () => {
      logout();
    };

    processTokensBeforePageLoads()
      .then((isOk: boolean) => {
        if (isOk) {
          setLastCheckedPathname(pathname);
        } else {
          handleFail();
        }
      })
      .catch(e => {
        console.error(e);
        handleFail();
      });
  }, [pathname, tokenExpiresAt]);

  return (
    <Route
      {...rest}
      render={props => {
        if (
          pathname === lastCheckedPathname &&
          token
          /* 
          Token is being checked for existence here instead of inside of useEffect for a reason: 
          Token expiration time and token itself are set by separate dispatch events to state. 
          Therefore you can't check it inside of useEffect (it's gonna log you out when it shouldn't). 
          There could be a situation when token expiration time is set (and useEffect check passes), 
          but the token itself in not yet set. So, you have to check it here. 
          */
        ) {
          return <Component {...props} />;
        }

        return <FullScreenLoader />;
      }}
    />
  );
};

export default withRouter(connector(PrivateRoute));
