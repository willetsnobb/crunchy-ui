import React, { FC } from "react";
import CircularProgress, {
  CircularProgressProps
} from "@material-ui/core/CircularProgress";
import { withTheme, WithTheme } from "@material-ui/core";

const LoadingSpinner: FC<CircularProgressProps & WithTheme> = ({
  theme,
  ...rest
}) => {
  return (
    <CircularProgress
      style={{ margin: theme.spacing(3) }}
      color="secondary"
      {...rest}
    />
  );
};

export default withTheme(LoadingSpinner);
