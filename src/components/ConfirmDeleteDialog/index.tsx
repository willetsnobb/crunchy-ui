import React, { FC, ReactNode } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  DialogContentText
} from "@material-ui/core";
import { useTranslation } from "react-i18next";

export type ConfirmDeleteDialogProps = {
  onConfirmDelete: Function;
  onClose: Function;
  open: boolean;
  title: string;
  deleteButtonDisabled?: boolean;
  text?: ReactNode;
};

const ConfirmDeleteDialog: FC<ConfirmDeleteDialogProps> = ({
  onConfirmDelete,
  deleteButtonDisabled = false,
  onClose,
  open,
  title,
  text
}) => {
  const { t } = useTranslation();

  return (
    <Dialog
      open={open}
      onClose={() => {
        onClose();
      }}
    >
      <DialogTitle>{title}</DialogTitle>

      {text && (
        <DialogContent>
          <DialogContentText>{text}</DialogContentText>
        </DialogContent>
      )}

      <DialogActions>
        <Button
          onClick={() => {
            onClose();
          }}
          color="primary"
        >
          {t("cancel")}
        </Button>

        <Button
          disabled={deleteButtonDisabled}
          onClick={() => {
            onConfirmDelete();
            onClose();
          }}
          color="primary"
          autoFocus
        >
          {t("delete")}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDeleteDialog;
