import { useEffect, FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { setPath } from "../../actions/commonActions";

const connector = connect(null, { setPath });

type PathObserverProps = ConnectedProps<typeof connector> &
  RouteComponentProps<{}>;

const PathObserver: FC<PathObserverProps> = ({ match: { path }, setPath }) => {
  useEffect(() => {
    setPath(path);
  }, [path, setPath]);

  return null;
};

export default withRouter(connector(PathObserver));
