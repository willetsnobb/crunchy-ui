import React, { FC } from "react";
import { Grid, GridProps } from "@material-ui/core";

type OwnProps = {
  size?: "small" | "middle";
};

type GridItemProps = GridProps & OwnProps;

const GridItem: FC<GridItemProps> = ({
  size = "small",
  children,
  ...props
}) => {
  type SizePropValue = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;

  type SizeProps = {
    xs?: SizePropValue;
    sm?: SizePropValue;
    md?: SizePropValue;
    lg?: SizePropValue;
    xl?: SizePropValue;
  };

  const appropriateSizeProps = (): SizeProps => {
    switch (size) {
      case "small":
        return { xs: 12, sm: 6, md: 4, lg: 3, xl: 2 };
      case "middle":
        return { xs: 12, sm: 12, md: 12, lg: 6, xl: 6 };
    }
  };

  return (
    <Grid {...props} item {...appropriateSizeProps()}>
      {children}
    </Grid>
  );
};

export default GridItem;
