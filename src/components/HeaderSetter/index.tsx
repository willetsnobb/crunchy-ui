import { useEffect, FC } from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import { useTranslation } from "react-i18next";
import { setHeader } from "../../actions/commonActions";
import paths from "../../paths/paths";
import { uiDate } from "../../utils/uiUtils";

const connector = connect(null, { setHeader });

type HeaderSetterProps = ConnectedProps<typeof connector> &
  RouteComponentProps<{ date: string }>;

const HeaderSetter: FC<HeaderSetterProps> = ({
  setHeader,
  match: {
    path,
    params: { date }
  }
}) => {
  const { t } = useTranslation();

  useEffect(() => {
    const updateHeader = (path: string) => {
      switch (path) {
        case paths.EditFood.url:
          setHeader(t("editing_food"));
          break;
        case paths.Foods.url:
          setHeader(t("foods"));
          break;
        case paths.AddFood.url:
          setHeader(t("new_food"));
          break;
        case paths.Meals.url:
          setHeader(t("my_meals"));
          break;
        case paths.MealsByDate.url:
          setHeader(`${uiDate(date)} | ${t("my_meals")}`);
          break;
        case paths.EditMeal.url:
          setHeader(t(`editing_meal`));
          break;
        case paths.AddMeal.url:
          setHeader(t(`new_meal`));
          break;
        case paths.LeaveFeedback.url:
          setHeader(t("feedback"));
          break;
        default:
          setHeader(null);
          break;
      }
    };

    updateHeader(path);
  }, [path, date, setHeader, t]);

  return null;
};

export default withRouter(connector(HeaderSetter));
