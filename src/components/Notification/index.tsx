import React, { useEffect, useState, FC, SyntheticEvent } from "react";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import {
  Snackbar,
  SnackbarContent,
  makeStyles,
  Theme,
  IconButton
} from "@material-ui/core";
import { green, amber } from "@material-ui/core/colors";
import ErrorIcon from "@material-ui/icons/Error";
import InfoIcon from "@material-ui/icons/Info";
import CloseIcon from "@material-ui/icons/Close";
import WarningIcon from "@material-ui/icons/Warning";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import clsx from "clsx";

import { NotificationType } from "../../utils/enums";
import { Notification as NotificationObj, State } from "../../utils/types";
import paths from "../../paths/paths";

export interface MySnackbarContentWrapperProps {
  className?: string;
  message?: string;
  onClose?: any;
  type: NotificationType;
}

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon
};

const useStyles1 = makeStyles((theme: Theme) => ({
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  info: {
    backgroundColor: theme.palette.primary.main
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
}));

type variantType = keyof typeof variantIcon;

function MySnackbarContentWrapper(props: MySnackbarContentWrapperProps) {
  const classes = useStyles1();
  const { className, message, onClose, type, ...other } = props;

  if (!type) {
    return null;
  }

  const getVariantByType = (type: NotificationType) =>
    `${type.toLocaleLowerCase()}`;

  const variant: variantType = getVariantByType(type) as variantType;

  const Icon: any = variantIcon[variant];

  return (
    <SnackbarContent
      className={clsx(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="close"
          color="inherit"
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>
      ]}
      {...other}
    />
  );
}

const useSnackbarStyles = makeStyles((theme: Theme) => ({
  snackbar: {
    [theme.breakpoints.down("xs")]: {
      bottom: 100
    }
  }
}));

const pathsWithFab = [paths.Foods.url, paths.Meals.url, paths.MealsByDate.url];

const isPageWithFab = (currentPath: string) =>
  pathsWithFab.some(testPath => testPath === currentPath);

interface NotificationProps {
  notification: NotificationObj | null;
  path: string;
}

const Notification: FC<NotificationProps> = ({ notification, path }) => {
  const { t } = useTranslation();

  const classes = useSnackbarStyles();

  const [open, setOpen] = useState<boolean>(false);

  const snackbarClass = isPageWithFab(path) ? classes.snackbar : "";

  useEffect(() => {
    if (notification) {
      setOpen(true);
    } else {
      setOpen(false);
    }
  }, [notification]);

  if (!notification || !t) {
    return null;
  }

  const handleClose = (e: SyntheticEvent<any, Event>, reason: string) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <Snackbar
      className={snackbarClass}
      open={open}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left"
      }}
      onClose={handleClose}
      autoHideDuration={6000}
    >
      <MySnackbarContentWrapper
        type={notification.type}
        message={t(notification.message)}
        onClose={handleClose}
      />
    </Snackbar>
  );
};

const mapStateToProps = (state: State) => ({
  notification: state.common.notification,
  path: state.common.path
});

export default connect(mapStateToProps)(Notification);
