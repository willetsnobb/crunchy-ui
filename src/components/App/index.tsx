import React, { useEffect, FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { Drawer } from "@material-ui/core";
import _ from "lodash";
import { setLoginUser } from "../../actions/loginActions";
import { setDrawerOpen } from "../../actions/commonActions";
import Routes from "../Routes";
import Notification from "../Notification";
import { getLSKey } from "../../utils/localstorage";
import { State } from "../../utils/types";
import { LSKey } from "../../utils/enums";
import DrawerContent from "../DrawerContent";
import WidthObserver from "../WidthObserver";

const connector = connect(
  (state: State) => ({
    userInfo: state.login.user,
    loggedIn: !!state.login.user,
    drawerOpen: state.common.drawerOpen,
    persistDrawer: state.common.persistDrawer
  }),
  {
    setLoginUser,
    setDrawerOpen
  }
);

type AppProps = ConnectedProps<typeof connector>;

const App: FC<AppProps> = ({
  userInfo,
  loggedIn,
  drawerOpen,
  persistDrawer,
  setLoginUser,
  setDrawerOpen
}) => {
  const user = getLSKey(LSKey.USER, null);

  useEffect(() => {
    if (user) {
      if (!_.isEqual(user, userInfo)) {
        setLoginUser(user);
      }
    }
  }, [user, userInfo, setLoginUser]);

  useEffect(() => {
    if (!userInfo) {
      setDrawerOpen(false);
    }
  }, [setDrawerOpen, userInfo]);

  return (
    <>
      {loggedIn && !persistDrawer && (
        <Drawer
          variant="temporary"
          onClose={() => setDrawerOpen(false)}
          open={drawerOpen}
        >
          <DrawerContent />
        </Drawer>
      )}

      <WidthObserver />

      <Routes />

      <Notification />
    </>
  );
};

export default connector(App);
