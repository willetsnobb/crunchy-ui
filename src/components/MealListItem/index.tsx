import React, { FC } from "react";
import {
  Card,
  CardContent,
  Typography,
  CardActions,
  Button
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import qs from "query-string";
import { Meal } from "../../utils/types";
import { uiNumber, uiTime, uiDateTime } from "../../utils/uiUtils";
import paths from "../../paths/paths";
import { Measurement } from "../../utils/enums";
import { SearchParameters } from "../../pages/AddEditMeal";

type MealListItemProps = {
  item: Meal;
  showDate?: boolean;
};

const MealListItem: FC<MealListItemProps> = ({
  item: meal,
  showDate = false
}) => {
  const { t } = useTranslation();

  const uiAmountUnitFromMeal = (meal: Meal) => {
    if (meal.measurement === Measurement["100_GRAM"]) {
      return t("grams");
    } else if (meal.measurement === Measurement["1_PORTION"]) {
      return t("portions");
    }
  };

  const mealTime = showDate
    ? uiDateTime(meal.timestamp)
    : uiTime(meal.timestamp);

  const repeatUrlSearchParams: SearchParameters = {
    preselected_food_id: "" + meal.food_id,
    preselected_amount_expression: meal.amount_expression || `${meal.amount}`
  };

  return (
    <Card>
      <CardContent style={{ paddingBottom: 0 }}>
        <Typography variant="h6">{mealTime}</Typography>
        <Typography>
          {meal.name},{" "}
          {`${uiNumber(meal.amount)} ${uiAmountUnitFromMeal(meal)}`}
        </Typography>
        <Typography>
          {t("kcal")}: {uiNumber(meal.kcal)}
        </Typography>
        <Typography>
          {t("prots")}: {uiNumber(meal.prots)}
          {t("grams_acronym")}
        </Typography>
        <Typography>
          {t("fats")}: {uiNumber(meal.fats)}
          {t("grams_acronym")}
        </Typography>
        <Typography>
          {t("carbs")}: {uiNumber(meal.carbs)}
          {t("grams_acronym")}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          component={Link}
          to={paths.EditMeal.toPath({
            mealId: meal.id
          })}
        >
          {t("edit")}
        </Button>
        <Button
          component={Link}
          to={`${paths.AddMeal.toPath()}?${qs.stringify(
            repeatUrlSearchParams
          )}`}
        >
          {t("repeat")}
        </Button>
      </CardActions>
    </Card>
  );
};

export default MealListItem;
