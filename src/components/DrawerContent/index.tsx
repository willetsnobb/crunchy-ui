import React, { FC, ChangeEvent } from "react";
import { Link } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import { useTranslation } from "react-i18next";
import {
  Typography,
  List,
  ListItem,
  ListItemText,
  Divider,
  makeStyles,
  Theme,
  FormControl,
  FormControlLabel,
  Switch
} from "@material-ui/core";
import { setDrawerOpen, setTheme } from "../../actions/commonActions";
import paths from "../../paths/paths";
import { State } from "../../utils/types";
import { ThemeVariant } from "../../utils/enums";
import { logout } from "../../services/authService";

const useStyles = makeStyles((theme: Theme) => ({
  drawerList: {
    width: 250
  },
  drawerTitle: {
    padding: theme.spacing(2),
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis"
  }
}));

const connector = connect(
  (state: State) => ({
    userInfo: state.login.user,
    darkTheme: state.common.theme === ThemeVariant.DARK,
    persistDrawer: state.common.persistDrawer
  }),
  {
    setDrawerOpen,
    setTheme
  }
);

type ReduxProps = ConnectedProps<typeof connector>;

type DrawerContentProps = ReduxProps;

const DrawerContent: FC<DrawerContentProps> = ({
  userInfo,
  persistDrawer,
  setDrawerOpen,
  setTheme,
  darkTheme
}) => {
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <div className={classes.drawerList} role="presentation">
      <Typography className={classes.drawerTitle} variant="h6">
        {userInfo && userInfo.login}
      </Typography>

      <FormControl>
        <FormControlLabel
          labelPlacement="start"
          control={
            <Switch
              checked={darkTheme}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setTheme(
                  e.target.checked ? ThemeVariant.DARK : ThemeVariant.LIGHT
                )
              }
              value="dark_theme"
            />
          }
          label={t("dark_theme")}
        />
      </FormControl>

      <List>
        <ListItem
          button
          component={Link}
          to={paths.Main.toPath()}
          onClick={() => setDrawerOpen(persistDrawer)}
        >
          <ListItemText primary={t("main_page")} />
        </ListItem>

        <ListItem
          button
          component={Link}
          onClick={() => setDrawerOpen(persistDrawer)}
          to={paths.Foods.toPath()}
        >
          <ListItemText primary={t("foods")} />
        </ListItem>

        <ListItem
          button
          component={Link}
          onClick={() => setDrawerOpen(persistDrawer)}
          to={paths.Meals.toPath()}
        >
          <ListItemText primary={t("my_meals")} />
        </ListItem>
      </List>

      <Divider />

      <List>
        <ListItem
          button
          component={Link}
          onClick={() => setDrawerOpen(persistDrawer)}
          to={paths.LeaveFeedback.toPath()}
        >
          <ListItemText primary={t("leave_feedback")} />
        </ListItem>
      </List>

      <Divider />

      <List>
        <ListItem
          button
          onClick={() => {
            logout();
          }}
        >
          <ListItemText primary={t(`logout`)} />
        </ListItem>
      </List>
    </div>
  );
};

export default connector(DrawerContent);
