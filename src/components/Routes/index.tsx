import React from "react";
import { Route, Switch } from "react-router-dom";

import paths from "../../paths/paths";
import PrivateRoute from "../PrivateRoute";

import Register from "../../pages/Register";
import Login from "../../pages/Login";
import MealsByDate from "../../pages/MealsByDate";
import AddEditFood from "../../pages/AddEditFood";
import Confirm from "../../pages/Confirm";
import ResetPassword from "../../pages/ResetPassword";
import ConfirmPasswordReset from "../../pages/ConfirmPasswordReset";
import Main from "../../pages/Main";
import Meals from "../../pages/Meals";
import AddEditMeal from "../../pages/AddEditMeal";
import Foods from "../../pages/Foods";
import LeaveFeedback from "../../pages/LeaveFeedback";
import NotFound from "../../pages/NotFound";
import PrivacyPolicy from "../../pages/PrivacyPolicy";
import ViewFood from "../../pages/ViewFood";
import MealsByFood from "../../pages/MealsByFood";

const Routes = () => (
  <Switch>
    <Route exact path={paths.Main.url} component={Main} />
    <Route exact path={paths.ViewPublicFood.url} component={ViewFood} />
    <PrivateRoute exact path={paths.Foods.url} component={Foods} />
    <PrivateRoute
      exact
      path={paths.LeaveFeedback.url}
      component={LeaveFeedback}
    />
    <PrivateRoute exact path={paths.AddFood.url} component={AddEditFood} />
    <PrivateRoute exact path={paths.EditFood.url} component={AddEditFood} />
    <PrivateRoute exact path={paths.Meals.url} component={Meals} />
    <PrivateRoute exact path={paths.MealsByDate.url} component={MealsByDate} />
    <PrivateRoute exact path={paths.AddMeal.url} component={AddEditMeal} />
    <PrivateRoute exact path={paths.EditMeal.url} component={AddEditMeal} />
    <PrivateRoute exact path={paths.MealsByFood.url} component={MealsByFood} />
    <Route exact path={paths.Register.url} component={Register} />
    <Route exact path={paths.Confirm.url} component={Confirm} />
    <Route exact path={paths.Login.url} component={Login} />
    <Route exact path={paths.ResetPassword.url} component={ResetPassword} />
    <Route
      exact
      path={paths.ConfirmPasswordReset.url}
      component={ConfirmPasswordReset}
    />
    <Route exact path={paths.PrivacyPolicy.url} component={PrivacyPolicy} />
    <Route component={NotFound} />
  </Switch>
);

export default Routes;
