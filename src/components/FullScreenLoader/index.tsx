import React, { FC } from "react";
import { makeStyles } from "@material-ui/core";
import LoadingSpinner from "../LoadingSpinner";

const useStyles = makeStyles(theme => ({
  wrapper: {
    width: "100vw",
    height: "100vh",
    position: "absolute",
    left: 0,
    top: 0,
    display: "flex",
    background: theme.palette.background.default
  },
  centeredSpinnerHolder: {
    margin: "auto"
  }
}));

type FullScreenLoaderProps = {};

const FullScreenLoader: FC<FullScreenLoaderProps> = () => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.centeredSpinnerHolder}>
        <LoadingSpinner />
      </div>
    </div>
  );
};

export default FullScreenLoader;
