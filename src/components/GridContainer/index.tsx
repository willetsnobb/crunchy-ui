import React, { FC } from "react";
import { Grid, GridProps } from "@material-ui/core";

const GridContainer: FC<GridProps> = ({ children, ...props }) => {
  return (
    <Grid {...props} container spacing={3}>
      {children}
    </Grid>
  );
};

export default GridContainer;
