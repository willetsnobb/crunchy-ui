import { compile } from "path-to-regexp";

interface UrlInstanceInterface {
  url: string;
  toPath: Function;
}

class UrlInstance implements UrlInstanceInterface {
  url = "";
  toPath = ({ ...args }: any = {}) => {
    const createPath = compile(this.url);

    return createPath({ ...args });
  };

  constructor(url: string) {
    this.url = url;
  }
}

export default {
  Main: new UrlInstance("/"),
  PrivacyPolicy: new UrlInstance("/privacy-policy"),
  Login: new UrlInstance("/login"),
  ResetPassword: new UrlInstance("/reset-password"),
  ConfirmPasswordReset: new UrlInstance("/confirm-password-reset"),
  Register: new UrlInstance("/register"),
  Confirm: new UrlInstance("/confirm"),
  LeaveFeedback: new UrlInstance("/leave-feedback"),
  Foods: new UrlInstance("/foods"),
  AddFood: new UrlInstance("/food/new"),
  EditFood: new UrlInstance("/food/:foodId"),
  MealsByFood: new UrlInstance("/food/:foodId/meals"),
  ViewPublicFood: new UrlInstance("/nutrition-value/:foodId"),
  Meals: new UrlInstance("/meals"),
  MealsByDate: new UrlInstance("/meals/:date"),
  AddMeal: new UrlInstance("/meal/new"),
  EditMeal: new UrlInstance("/meal/:mealId")
};
