import axios from "../utils/axios";
import endpoints from "../utils/endpoints";
import { Meal, Food } from "../utils/types";

export default {
  add,
  update,
  get,
  getByTimestamps,
  getByFood,
  delete: remove
};

function add(meal: Meal) {
  return axios.post(endpoints.POST_MEAL, meal);
}

function update(meal: Meal) {
  return axios.put(endpoints.PUT_MEAL, meal);
}

function get(mealId: Meal["id"]) {
  return axios.get(endpoints.GET_MEAL, { params: { id: mealId } });
}

function getByTimestamps({
  from_inclusive,
  to_inclusive
}: {
  from_inclusive: string;
  to_inclusive: string;
}) {
  return axios.get(endpoints.GET_MEAL_LIST_BY_TIMESTAMPS, {
    params: { from_inclusive, to_inclusive }
  });
}

function getByFood(foodId: Food["id"]) {
  return axios.get(endpoints.GET_MEAL_LIST_BY_FOOD, {
    params: {
      food_id: foodId
    }
  });
}

function remove(mealId: Meal["id"]) {
  return axios.delete(endpoints.DELETE_MEAL, {
    data: { id: mealId }
  });
}
