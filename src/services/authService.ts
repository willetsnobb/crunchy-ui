import qs from "query-string";
import jwt from "jsonwebtoken";
import store from "./../utils/store";
import {
  _setToken,
  _dropStateOnLogout,
  _setTokenExpiresAt
} from "../actions/loginActions";
import { putLSKey, deleteLSKey } from "../utils/localstorage";
import { LSKey } from "../utils/enums";
import history from "../utils/history";
import paths from "../paths/paths";
import { getUnixSecTime } from "../utils/time";

export const logout = (redirectToLogin = true) => {
  deleteLSKey(LSKey.REFRESH_TOKEN);
  deleteLSKey(LSKey.USER);
  deleteLSKey(LSKey.REFRESH_TOKEN_EXPIRES_AT);

  store.dispatch(_dropStateOnLogout());

  const { hash, pathname } = window.location;

  const currentLocation = pathname + hash.replace("#/", "");

  if (redirectToLogin) {
    history.push(
      `${paths.Login.toPath()}?${qs.stringify({ bounce_to: currentLocation })}`
    );
  }
};

export type TokensType = {
  token: string;
  refresh_token: string;
};

export const setTokens = ({ token, refresh_token }: TokensType) => {
  const processToken = (token: string, save: Function) => {
    const decodedToken: any = jwt.decode(token);
    const unixSecTime = getUnixSecTime();

    const expiresAt: number = unixSecTime + Number(decodedToken.expiresIn);

    save(expiresAt);
  };

  try {
    processToken(token, (expiresAt: number) => {
      store.dispatch(_setTokenExpiresAt(expiresAt));
    });

    processToken(refresh_token, (expiresAt: number) => {
      putLSKey(LSKey.REFRESH_TOKEN_EXPIRES_AT, expiresAt);
    });

    putLSKey(LSKey.REFRESH_TOKEN, refresh_token);

    store.dispatch(_setToken(token));
  } catch (e) {
    console.error(e);
  }
};
