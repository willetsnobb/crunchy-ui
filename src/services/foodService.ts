import axios from "../utils/axios";
import endpoints from "../utils/endpoints";
import { Food } from "../utils/types";
import { Impression } from "../utils/enums";

export default {
  create,
  get,
  getAll,
  getAllPublic,
  getPublic,
  delete: remove,
  update,
  searchPublicFoods,
  copyPublicFood,
  getImpressions,
  addImpression,
  removeImpression
};

function create(food: Food) {
  return axios.post(endpoints.POST_FOOD, food);
}

function get(foodId: Food["id"]) {
  return axios.get(endpoints.GET_FOOD, { params: { id: foodId } });
}

function getPublic(foodId: Food["id"]) {
  return axios.get(endpoints.GET_PUBLIC_FOOD, { params: { id: foodId } });
}

function getAllPublic() {
  return axios.get(endpoints.GET_FOOD_LIST_PUBLIC);
}

function getAll() {
  return axios.get(endpoints.GET_FOOD_LIST);
}

function remove(foodId: Food["id"]) {
  return axios.delete(endpoints.DELETE_FOOD, { data: { id: foodId } });
}

function update(food: Food) {
  return axios.put(endpoints.PUT_FOOD, food);
}

function searchPublicFoods(searchText: string) {
  return axios.get(endpoints.GET_FOOD_SEARCH, { params: { name: searchText } });
}

function copyPublicFood(foodId: Food["id"]) {
  return axios.post(endpoints.POST_COPY_FOOD, { id: foodId });
}

function getImpressions(foodId: Food["id"]) {
  return axios.get(endpoints.GET_FOOD_IMPRESSIONS, {
    params: { food_id: foodId }
  });
}

function addImpression(foodId: Food["id"], impression: Impression) {
  return axios.post(endpoints.POST_FOOD_IMPRESSION, {
    food_id: foodId,
    impression_type: impression
  });
}

function removeImpression(foodId: Food["id"]) {
  return axios.delete(endpoints.DELETE_FOOD_IMPRESSION, {
    data: { food_id: foodId }
  });
}
