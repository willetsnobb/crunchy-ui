import "core-js/stable";
import "regenerator-runtime/runtime";
import React from "react";
import ReactDOM from "react-dom";
import { HashRouter, BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import MomentUtils from "@date-io/moment";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import store from "./utils/store";
import "./index.css";
import "./utils/i18n";
import ThemedApp from "./components/ThemedApp";

const Router = process.env.REACT_APP_HASHROUTER ? HashRouter : BrowserRouter;

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <ThemedApp />
      </MuiPickersUtilsProvider>
    </Router>
  </Provider>,
  document.getElementById("root")
);

const versionComment = document.createComment(
  `UI v${process.env.REACT_APP_VERSION}`
);

document.body.appendChild(versionComment);
