import constants from "../utils/constants";
import { Dispatch } from "react";
import { Notification, Action, CommonReducerState } from "../utils/types";
import { ThemeVariant } from "../utils/enums";

export const setHeader = (header: CommonReducerState["header"]) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setHeader(header));
};

export const setTheme = (themeVariant: ThemeVariant) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setTheme(themeVariant));
};

export const setDrawerOpen = (isOpen: CommonReducerState["drawerOpen"]) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setDrawerOpen(isOpen));
};

export const _setDrawerOpen = (
  isOpen: CommonReducerState["drawerOpen"]
): Action => ({
  payload: isOpen,
  type: constants.SET_DRAWER_OPEN
});

const _setHeader = (header: CommonReducerState["header"]) => ({
  payload: header,
  type: constants.SET_HEADER
});

const _setTheme = (theme: ThemeVariant) => ({
  type: constants.SET_THEME,
  payload: theme
});

export const setNotification = (notification: Notification) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setNotification(notification));
};

export const _setNotification = (notification: Notification) => ({
  payload: notification,
  type: constants.SET_NOTIFICATION
});

const _setPath = (path: string) => ({
  payload: path,
  type: constants.SET_PATH
});

export const setPath = (path: string) => (dispatch: Dispatch<Action>) => {
  dispatch(_setPath(path));
};

const _setPersistDrawer = (persist: boolean) => ({
  payload: persist,
  type: constants.SET_PERSIST_DRAWER
});

export const setPersistDrawer = (persist: boolean) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setPersistDrawer(persist));
};

const _setUseTables = (shouldWe: CommonReducerState["useTables"]) => ({
  payload: shouldWe,
  type: constants.SET_USE_TABLES
});

export const setUseTables = (shouldWe: CommonReducerState["useTables"]) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setUseTables(shouldWe));
};
