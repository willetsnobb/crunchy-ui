import constants from "../utils/constants";
import { _setNotification } from "./commonActions";
import { NotificationType } from "../utils/enums";
import { Food, Action } from "../utils/types";
import { Dispatch } from "react";
import { uiNumber } from "../utils/uiUtils";
import foodService from "../services/foodService";

const addPercents = (food: Food): Food => {
  const prots = Number(food.prots);
  const fats = Number(food.fats);
  const carbs = Number(food.carbs);
  const kcal = Number(food.kcal);

  const total = prots + fats + carbs;

  const percents = { prots: 0, fats: 0, carbs: 0 };

  if (total > 0) {
    percents.prots = uiNumber((prots / total) * 100);
    percents.fats = uiNumber((fats / total) * 100);
    percents.carbs = uiNumber(100 - percents.prots - percents.fats);
  }

  return {
    ...food,

    protsPercent: percents.prots,
    fatsPercent: percents.fats,
    carbsPercent: percents.carbs,

    prots,
    fats,
    carbs,
    kcal
  };
};

const responseFoodToLocalModel = (item: Food): Food => {
  const localModel: Food = addPercents(item);

  return localModel;
};

export const copyFood = (foodId: Food["id"]) => {
  const request = () => ({
    payload: foodId,
    type: constants.FOOD_COPY_REQUEST
  });
  const fail = () => ({
    payload: foodId,
    type: constants.FOOD_COPY_FAIL
  });
  const success = (food: Food) => ({
    type: constants.FOOD_COPY_SUCCESS,
    payload: { food, foodId }
  });

  return (dispatch: Dispatch<Action>) => {
    dispatch(request());

    foodService
      .copyPublicFood(foodId)
      .then(response => {
        const copy: Food = response && response.data && response.data.data;

        dispatch(success(responseFoodToLocalModel(copy)));

        dispatch(
          _setNotification({
            type: NotificationType.SUCCESS,
            message: "copied_to_your_foods"
          })
        );
      })
      .catch(e => {
        console.error(e);

        dispatch(
          _setNotification({
            type: NotificationType.ERROR,
            message: "failed"
          })
        );

        dispatch(fail());
      });
  };
};

export const getFoodsList = () => {
  const request = () => ({
    type: constants.FOOD_GET_ALL_REQUEST
  });
  const fail = () => ({
    type: constants.FOOD_GET_ALL_FAIL
  });
  const success = (foods: Food[]) => ({
    type: constants.FOOD_GET_ALL_SUCCESS,
    payload: foods
  });

  return (dispatch: Dispatch<Action>) => {
    dispatch(request());

    foodService
      .getAll()
      .then(response => {
        const foods: Food[] = response && response.data && response.data.data;

        dispatch(
          success(foods.map((item: Food) => responseFoodToLocalModel(item)))
        );
      })
      .catch(e => {
        console.error(e);

        dispatch(
          _setNotification({
            type: NotificationType.ERROR,
            message: "failed_to_load_resource"
          })
        );

        dispatch(fail());
      });
  };
};

export const postFood = (food: Food, onSuccess?: Function) => {
  const request = () => ({
    type: constants.FOOD_CREATE_REQUEST
  });
  const success = (food: Food) => ({
    payload: food,
    type: constants.FOOD_CREATE_SUCCESS
  });
  const fail = () => ({
    type: constants.FOOD_CREATE_FAIL
  });

  return (dispatch: Dispatch<Action>) => {
    dispatch(request());

    foodService
      .create(food)
      .then(response => {
        const item: Food = response && response.data && response.data.data;

        dispatch(success(responseFoodToLocalModel(item)));

        dispatch(
          _setNotification({
            type: NotificationType.SUCCESS,
            message: "added"
          })
        );

        if (onSuccess) {
          onSuccess();
        }
      })
      .catch(e => {
        console.error(e);

        dispatch(
          _setNotification({
            type: NotificationType.ERROR,
            message: "failed"
          })
        );

        dispatch(fail());
      });
  };
};

export const updateFood = (food: Food) => {
  const request = () => ({
    payload: food.id,
    type: constants.FOOD_UPDATE_REQUEST
  });
  const fail = () => ({
    payload: food.id,
    type: constants.FOOD_UPDATE_FAIL
  });
  const success = (food: Food) => ({
    payload: food,
    type: constants.FOOD_UPDATE_SUCCESS
  });

  return (dispatch: Dispatch<Action>) => {
    dispatch(request());

    foodService
      .update(food)
      .then(response => {
        const updatedFood: Food =
          response && response.data && response.data.data;

        dispatch(
          _setNotification({
            type: NotificationType.SUCCESS,
            message: "updated"
          })
        );

        dispatch(success(responseFoodToLocalModel(updatedFood)));
      })
      .catch(e => {
        console.error(e);

        dispatch(
          _setNotification({
            type: NotificationType.ERROR,
            message: "failed"
          })
        );

        dispatch(fail());
      });
  };
};
