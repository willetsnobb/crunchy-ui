import { Dispatch } from "react";
import constants from "../utils/constants";
import { User, Action, LoginReducerState } from "../utils/types";

export const setLoginUser = (user: User | null) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setLoginUser(user));
};

export const dropStateOnLogout = () => (dispatch: Dispatch<Action>) => {
  dispatch(_dropStateOnLogout());
};

export const _dropStateOnLogout = (): Action => ({
  type: constants.DROP_STATE_ON_LOGOUT
});

const _setLoginUser = (user: User | null): Action => ({
  payload: user,
  type: constants.SET_LOGIN_USER
});

export const _setToken = (token: LoginReducerState["token"]): Action => ({
  payload: token,
  type: constants.SET_TOKEN
});

export const setToken = (token: LoginReducerState["token"]) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setToken(token));
};

export const _setTokenExpiresAt = (
  at: LoginReducerState["tokenExpiresAt"]
): Action => ({
  payload: at,
  type: constants.SET_TOKEN_EXPIRES_AT
});
