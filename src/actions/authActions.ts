import axios from "./../utils/axios";
import endpoints from "../utils/endpoints";
import { getLSKey } from "../utils/localstorage";
import { LSKey } from "../utils/enums";

export const refreshToken = () => {
  return axios.get(endpoints.GET_AUTH_REFRESH_TOKEN, {
    params: {
      refresh_token: getLSKey(LSKey.REFRESH_TOKEN)
    }
  });
};
