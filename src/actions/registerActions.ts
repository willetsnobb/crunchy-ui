import { Dispatch } from "redux";
import constants from "../utils/constants";
import axios from "./../utils/axios";
import endpoints from "../utils/endpoints";
import {
  responseIsSuccessful,
  errorIsLoginTaken,
  responseIsEmailSent,
  errorIsEmailNotSent
} from "../utils/variousUtils";
import { _setNotification } from "./commonActions";
import { RequestStatus, ResponseCodes, NotificationType } from "../utils/enums";
import { Action } from "../utils/types";

interface RegData {
  login: string;
  password: string;
}

export const register = (data: RegData, successCallback?: Function) => async (
  dispatch: Dispatch<Action>
) => {
  try {
    dispatch(_setRegisterRequestStatus(RequestStatus.FETCHING));

    const response = await axios.post(endpoints.POST_USER_REGISTER, data);

    if (responseIsSuccessful(response) && responseIsEmailSent(response)) {
      dispatch(_setRegisterRequestStatus(ResponseCodes.EMAIL_SENT));
      dispatch(
        _setNotification({
          type: NotificationType.SUCCESS,
          message: "email_sent"
        })
      );

      if (successCallback) {
        successCallback();
      }
    } else {
      dispatch(_setRegisterRequestStatus(RequestStatus.FAIL));
      dispatch(
        _setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        })
      );
    }
  } catch (e) {
    if (errorIsEmailNotSent(e)) {
      dispatch(_setRegisterRequestStatus(ResponseCodes.EMAIL_NOT_SENT));
      dispatch(
        _setNotification({
          type: NotificationType.ERROR,
          message: "error_occured_while_sending_email"
        })
      );
    } else if (errorIsLoginTaken(e)) {
      dispatch(_setRegisterRequestStatus(ResponseCodes.LOGIN_TAKEN));
      dispatch(
        _setNotification({
          type: NotificationType.ERROR,
          message: "login_taken"
        })
      );
    } else {
      dispatch(_setRegisterRequestStatus(RequestStatus.FAIL));
      dispatch(
        _setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        })
      );
    }
  }
};

const _setRegisterRequestStatus = (status: RequestStatus | ResponseCodes) => ({
  payload: status,
  type: constants.SET_REGISTER_REQUEST_STATUS
});
