import { Dispatch } from "react";
import constants from "../utils/constants";
import history from "../utils/history";
import { responseIsSuccessful } from "../utils/variousUtils";
import { _setNotification } from "./commonActions";
import paths from "../paths/paths";
import {
  RequestStatus,
  NotificationType,
  MealIntervalOption
} from "../utils/enums";
import { Meal, Action } from "../utils/types";
import moment, { Moment } from "moment";
import { backendDateTimeFormat } from "../utils/backendUtils";
import mealService from "../services/mealService";

const _setMealInterval = (intervalOption: MealIntervalOption): Action => ({
  payload: intervalOption,
  type: constants.SET_MEAL_INTERVAL
});

export const setMealInterval = (intervalOption: MealIntervalOption) => (
  dispatch: Dispatch<Action>
) => {
  dispatch(_setMealInterval(intervalOption));
};

export enum GetMealsType {
  BY_INTERVAL,
  BY_DATE
}
export type GetMealsOptions = {
  intervalOption?: MealIntervalOption;
  date?: string;
};
export const getMealsList = (
  type: GetMealsType = GetMealsType.BY_INTERVAL,
  { intervalOption = MealIntervalOption.LIFETIME, date }: GetMealsOptions
) => async (dispatch: Dispatch<Action>) => {
  interface Params {
    from_inclusive: string;
    to_inclusive: string;
  }

  function calcIntervals(intervalOption: MealIntervalOption): Params {
    let from_inclusive: Moment = moment();
    let to_inclusive: Moment = moment();

    if (type === GetMealsType.BY_INTERVAL) {
      if (intervalOption === MealIntervalOption.LIFETIME) {
        from_inclusive = moment(0);
        to_inclusive = moment().endOf("day");
      } else if (intervalOption === MealIntervalOption.THIS_WEEK) {
        from_inclusive = moment().startOf("week");
        to_inclusive = moment().endOf("week");
      } else if (intervalOption === MealIntervalOption.THIS_MONTH) {
        from_inclusive = moment().startOf("month");
        to_inclusive = moment().endOf("month");
      } else if (intervalOption === MealIntervalOption.LAST_WEEK) {
        from_inclusive = moment()
          .subtract(1, "week")
          .startOf("week");
        to_inclusive = moment()
          .subtract(1, "week")
          .endOf("week");
      } else if (intervalOption === MealIntervalOption.LAST_MONTH) {
        from_inclusive = moment()
          .subtract(1, "month")
          .startOf("month");
        to_inclusive = moment()
          .subtract(1, "month")
          .endOf("month");
      }
    } else if (type === GetMealsType.BY_DATE) {
      from_inclusive = moment(date).startOf("day");
      to_inclusive = moment(date).endOf("day");
    }

    return {
      from_inclusive: from_inclusive.format(backendDateTimeFormat).toString(),
      to_inclusive: to_inclusive.format(backendDateTimeFormat).toString()
    };
  }

  dispatch(_setMealsGetRequestStatus(RequestStatus.FETCHING));

  mealService
    .getByTimestamps(calcIntervals(intervalOption))
    .then(response => {
      if (responseIsSuccessful(response)) {
        dispatch(_setMealsList(response.data.data));
        dispatch(_setMealsGetRequestStatus(RequestStatus.SUCCESS));
      } else {
        dispatch(_setMealsGetRequestStatus(RequestStatus.FAIL));
        dispatch(
          _setNotification({
            type: NotificationType.ERROR,
            message: "failed_to_load_resource"
          })
        );
      }
    })
    .catch(e => {
      console.error(e);
      dispatch(
        _setNotification({
          type: NotificationType.ERROR,
          message: "failed_to_load_resource"
        })
      );
      dispatch(_setMealsGetRequestStatus(RequestStatus.FAIL));
    });
};

export const postMeal = (meal: Meal) => async (dispatch: Dispatch<Action>) => {
  dispatch(_setMealPostRequestStatus(RequestStatus.FETCHING));

  mealService
    .add(meal)
    .then(response => {
      if (responseIsSuccessful(response)) {
        dispatch(_addMeal(response.data.data));
        dispatch(
          _setNotification({
            type: NotificationType.SUCCESS,
            message: "added"
          })
        );
        dispatch(_setMealPostRequestStatus(RequestStatus.SUCCESS));
      } else {
        dispatch(
          _setNotification({
            type: NotificationType.ERROR,
            message: "failed"
          })
        );
        dispatch(_setMealPostRequestStatus(RequestStatus.FAIL));
      }
    })
    .catch(e => {
      console.error(e);
      dispatch(
        _setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        })
      );
      dispatch(_setMealPostRequestStatus(RequestStatus.FAIL));
    });
};

export const editMeal = (meal: Meal) => async (dispatch: Dispatch<Action>) => {
  dispatch(_setMealPutRequestStatus(RequestStatus.FETCHING));

  mealService
    .update(meal)
    .then(response => {
      if (responseIsSuccessful(response)) {
        dispatch(_updateMeal(response.data.data));
        dispatch(
          _setNotification({
            type: NotificationType.SUCCESS,
            message: "updated"
          })
        );
        dispatch(_setMealPutRequestStatus(RequestStatus.SUCCESS));
      } else {
        dispatch(
          _setNotification({
            type: NotificationType.ERROR,
            message: "failed"
          })
        );
        dispatch(_setMealPutRequestStatus(RequestStatus.FAIL));
      }
    })
    .catch(e => {
      console.error(e);
      dispatch(
        _setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        })
      );
      dispatch(_setMealPutRequestStatus(RequestStatus.FAIL));
    });
};

export const deleteMeal = (
  id: string | number,
  redirectToMealsPage: boolean = true,
  onSuccess: Function = () => {}
) => (dispatch: Dispatch<Action>) => {
  const request = (): Action => ({
    type: constants.MEAL_DELETE_REQUEST,
    payload: id
  });
  const success = (): Action => ({
    type: constants.MEAL_DELETE_SUCCESS,
    payload: id
  });
  const fail = (): Action => ({
    type: constants.MEAL_DELETE_FAIL,
    payload: id
  });

  dispatch(request());

  mealService
    .delete(id)
    .then(response => {
      if (responseIsSuccessful(response)) {
        dispatch(
          _setNotification({
            type: NotificationType.SUCCESS,
            message: "deleted"
          })
        );
        dispatch(success());
        onSuccess();

        if (redirectToMealsPage) {
          history.push(paths.Meals.toPath());
        }
      }
    })
    .catch(e => {
      console.error(e);
      dispatch(
        _setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        })
      );
      dispatch(fail());
    });
};

const _setMealsGetRequestStatus = (status: RequestStatus) => ({
  payload: status,
  type: constants.SET_MEALS_GET_REQUEST_STATUS
});

const _setMealPostRequestStatus = (status: RequestStatus) => ({
  payload: status,
  type: constants.SET_MEAL_POST_REQUEST_STATUS
});

const _setMealPutRequestStatus = (status: RequestStatus) => ({
  payload: status,
  type: constants.SET_MEAL_PUT_REQUEST_STATUS
});

const _addMeal = (meal: Meal) => ({
  payload: meal,
  type: constants.ADD_MEAL
});

const _setMealsList = (list: Meal[]) => ({
  payload: list,
  type: constants.SET_MEALS_LIST
});

const _updateMeal = (meal: Meal) => ({
  payload: meal,
  type: constants.UPDATE_MEAL
});
