import { ResponseCodes } from "./enums";

const errorIs = (error: any, statusCode: any): boolean =>
  error &&
  error.response &&
  error.response.data &&
  error.response.data.status_code === statusCode;

export const responseIsSuccessful = (response: any) =>
  response && response.data && response.data.status_ok === true;

export const responseIsEmailSent = (response: any) =>
  response &&
  response.data &&
  response.data.status_code === ResponseCodes.EMAIL_SENT;

export const errorIsInvalidLoginPasswordCombination = (error: any) =>
  errorIs(error, ResponseCodes.INVALID_LOGIN_PASSWORD_COMBINATION);

export const errorIsTokenValidationFailed = (error: any) =>
  errorIs(error, ResponseCodes.TOKEN_VALIDATION_FAILED);

export const errorIsEmailNotSent = (error: any) =>
  errorIs(error, ResponseCodes.EMAIL_NOT_SENT);

export const errorIsEmailNotConfirmed = (error: any) =>
  errorIs(error, ResponseCodes.EMAIL_NOT_CONFIRMED);

export const errorIsLoginTaken = (error: any) =>
  errorIs(error, ResponseCodes.LOGIN_TAKEN);

export const errorIsNotFound = (error: any) =>
  errorIs(error, ResponseCodes.NOT_FOUND);

export const errorIsUnableUoProcessRefreshToken = (error: any) =>
  errorIs(error, ResponseCodes.UNABLE_TO_PROCESS_REFRESH_TOKEN);
