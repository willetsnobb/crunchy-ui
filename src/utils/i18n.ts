import i18next from "i18next";
import detector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
import en from "./../translations/en.json";

const resources = {
  en: {
    translation: en
  }
};

i18next
  .use(detector)
  .use(initReactI18next)
  .init({
    interpolation: {
      escapeValue: false
    },
    resources,
    fallbackLng: "en"
  });

export default i18next;
