import moment from "moment";
import i18next from "i18next";
import "moment/locale/ru";

const { language } = i18next;

export const uiNumber = (num: any): number => {
  try {
    return Number(parseFloat(num).toFixed(2));
  } catch {
    return num;
  }
};

export const uiDate = (time: any) =>
  moment(time)
    .locale(language)
    .format("ll");

export const uiTime = (time: any) =>
  moment(time)
    .locale(language)
    .format("LT");

export const uiDateTime = (time: any) =>
  moment(time)
    .locale(language)
    .format("lll");
