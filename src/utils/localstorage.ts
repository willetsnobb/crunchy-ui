import { LSKey } from "./enums";

type LSKeyType = string | LSKey;
type LSValueType = any | null;

export function putLSKey(key: LSKeyType, value: LSValueType) {
  try {
    localStorage.setItem(key, JSON.stringify(value));
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}

export function getLSKey(key: LSKeyType, defaultValue: LSValueType = "") {
  try {
    const value = localStorage.getItem(key);

    if (value === null && typeof value === "object") {
      // item is null if it's not found in localStorage
      return defaultValue;
    } else {
      return JSON.parse(value);
    }
  } catch (e) {
    console.error(e);
    return defaultValue;
  }
}

export function deleteLSKey(key: LSKeyType) {
  try {
    localStorage.removeItem(key);
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
}
