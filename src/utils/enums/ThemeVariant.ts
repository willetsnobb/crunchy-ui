export enum ThemeVariant {
  LIGHT = "LIGHT",
  DARK = "DARK"
}
