export enum LSKey {
  THEME_VARIANT = "cr_theme_variant",
  REFRESH_TOKEN = "cr_refresh_token",
  REFRESH_TOKEN_EXPIRES_AT = "cr_refresh_token_expires_at",
  USER = "cr_user"
}
