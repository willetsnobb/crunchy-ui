export enum RequestStatus {
  NOT_REQUESTED = "NOT_REQUESTED",
  FETCHING = "FETCHING",
  SUCCESS = "SUCCESS",
  FAIL = "FAIL"
}
