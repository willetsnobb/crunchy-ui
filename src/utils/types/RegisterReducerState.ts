
import { RequestStatus } from "../enums";

export interface RegisterReducerState {
  requestStatus: RequestStatus;
}
