import { Food } from "./Food";

export interface FoodsReducerState {
  list: Food[];
  creating: boolean;
  updating: boolean;
  loadingList: boolean;

  copying: Food["id"][]; // array of foodIds being copied
}
