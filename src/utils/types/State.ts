import { ReduxState } from "../../reducers/rootReducer";

export type State = ReduxState;
