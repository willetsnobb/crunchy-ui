import { User } from "./User";

export interface LoginReducerState {
  user: User | null;
  token: string | null;
  tokenExpiresAt: number;
}
