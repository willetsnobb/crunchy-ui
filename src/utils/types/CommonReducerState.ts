import { Notification } from "./Notification";
import { ThemeVariant } from "../enums";

export interface CommonReducerState {
  header: string | null;
  drawerOpen: boolean;
  useTables: boolean;
  persistDrawer: boolean;
  notification: Notification | null;
  theme: ThemeVariant;
  path: string;
}
