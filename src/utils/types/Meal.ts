import { Measurement } from "../enums";

export interface Meal {
  id?: number | string;

  user_id?: number | string;
  food_id: number | string;

  amount: number | string;

  timestamp: number | string;
  amount_expression: string | null;

  // Local mutations
  deleting?: boolean;

  // if food is fetched
  name?: string;
  about?: string;
  measurement?: Measurement;
  kcal?: number | string;
  prots?: number | string;
  fats?: number | string;
  carbs?: number | string;
  can_be_public?: boolean;
  public?: boolean;
  created_at?: number | string;
}
