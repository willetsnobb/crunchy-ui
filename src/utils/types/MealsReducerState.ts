import { RequestStatus, MealIntervalOption } from "../enums";
import { Meal } from "./Meal";

export interface MealsReducerState {
  list: Meal[];
  getRequestStatus: RequestStatus;
  postRequestStatus: RequestStatus;
  putRequestStatus: RequestStatus;
  deleteRequestStatus: RequestStatus;
  interval: MealIntervalOption
}
