export interface DailyMealsSummary {
  kcal: number;

  prots: number;
  fats: number;
  carbs: number;

  protsPercent: number;
  fatsPercent: number;
  carbsPercent: number;

  uiDate: string;
  date: string;
}
