import { Measurement, Impression } from "../enums";

export interface Food {
  id?: number | string;
  user_id?: number | string;

  name: string;
  about: string;
  public: boolean;

  measurement: Measurement;

  kcal: number | string;

  prots: number | string;
  fats: number | string;
  carbs: number | string;

  kcal_expression: string | null;

  prots_expression: string | null;
  fats_expression: string | null;
  carbs_expression: string | null;

  created_at?: string | number;
  can_be_public?: boolean;

  my_impression?: null | Impression;
  likes?: number;
  dislikes?: number;

  // local mutations

  loadingImpressions?: boolean;

  protsPercent?: number;
  fatsPercent?: number;
  carbsPercent?: number;
}
