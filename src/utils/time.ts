export const getUnixSecTime = () => {
  const unixSecTime = Math.floor(Date.now() / 1000);
  return unixSecTime;
};
