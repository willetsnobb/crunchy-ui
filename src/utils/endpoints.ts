export default {
  GET_AUTH_REFRESH_TOKEN: "/auth/refresh-token",

  POST_USER_REGISTER: "/user/register",
  POST_USER_LOGIN: "/user/login",
  POST_USER_CONFIRM: "/user/confirm",
  GET_CHECK_LOGIN: "/user/check-login",
  POST_RESEND_CONFIRMATION_EMAL: "/user/resend-confirmation-email",
  POST_USER_PASSWORD_RESET_REQUEST: "/user/password-reset-request",
  POST_USER_PASSWORD_RESET_CONFIRM: "/user/password-reset-confirm",

  POST_FOOD: "/food/",
  GET_FOOD: "/food/",
  GET_PUBLIC_FOOD: "/food/public/",
  PUT_FOOD: "/food/",
  DELETE_FOOD: "/food/",
  GET_FOOD_SEARCH: "/food/search/",
  POST_COPY_FOOD: "/food/copy/",
  GET_FOOD_LIST: "/food/list/",
  GET_FOOD_LIST_PUBLIC: "/food/list-public/",

  GET_FOOD_IMPRESSIONS: "/food/impressions",
  POST_FOOD_IMPRESSION: "/food/impression",
  DELETE_FOOD_IMPRESSION: "/food/impression",

  POST_MEAL: "/meal/",
  GET_MEAL: "/meal/",
  PUT_MEAL: "/meal/",
  DELETE_MEAL: "/meal/",
  GET_MEAL_LIST: "/meal/list/",
  GET_MEAL_LIST_BY_TIMESTAMPS: "/meal/list-by-timestamps/",
  GET_MEAL_LIST_BY_FOOD: "/meal/list-by-food/",
  DELETE_MEALS_BY_FOOD: "/meal/delete-by-food/",

  POST_FEEDBACK: "/feedback/"
};
