import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import createRootReducer from "../reducers/rootReducer";

export default createStore(
  createRootReducer,
  compose(applyMiddleware(thunk, logger))
);
