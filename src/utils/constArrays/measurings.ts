import { Measurement } from "../enums";

export default [Measurement["100_GRAM"], Measurement["1_PORTION"]];
