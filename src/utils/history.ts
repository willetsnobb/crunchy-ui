const createHashHistory = require("history").createHashHistory;
const createBrowserHistory = require("history").createBrowserHistory;

const createHistory = process.env.REACT_APP_HASHROUTER
  ? createHashHistory
  : createBrowserHistory;

export default createHistory();
