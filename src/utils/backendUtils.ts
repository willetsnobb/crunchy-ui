import moment from "moment";

export const backendDateTimeFormat = "YYYY-MM-DD HH:mm:ss.SSS Z";

export const backendZonedDateTime = (dateTime?: number | string) =>
  moment(dateTime)
    .format(backendDateTimeFormat)
    .toString();
