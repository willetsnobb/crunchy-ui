import axios, { AxiosInstance } from "axios";
import {
  errorIsTokenValidationFailed,
  errorIsUnableUoProcessRefreshToken
} from "./variousUtils";
import { getLSKey } from "./localstorage";
import { LSKey } from "./enums";
import { refreshToken } from "../actions/authActions";
import { State } from "./types";
import store from "../utils/store";
import { setTokens, logout, TokensType } from "../services/authService";

const API_PREFIX = process.env.REACT_APP_API_PREFIX || "/api";
const BACKEND_URL =
  process.env.REACT_APP_BACKEND_URL || "http://localhost:5005";

const headers = {
  Accept: "application/json",
  "Content-Type": "application/json"
};

const instance: AxiosInstance = axios.create({
  baseURL: `${BACKEND_URL}${API_PREFIX}`,
  withCredentials: false,
  headers
});

instance.interceptors.request.use(config => {
  const state: State = store.getState();
  const token = state.login.token;

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }

  return config;
});

instance.interceptors.response.use(
  r => r,
  err => {
    if (errorIsTokenValidationFailed(err)) {
      const handleFail = () => {
        logout();
      };

      if (getLSKey(LSKey.REFRESH_TOKEN)) {
        return refreshToken()
          .then((response: any) => {
            const tokens: TokensType =
              response && response.data && response.data.data;

            setTokens(tokens);

            const { token } = tokens;

            err.config.headers.Authorization = `Bearer ${token}`;

            err.config.url = err.config.url.replace(
              `/${process.env.REACT_APP_API_PREFIX}`,
              ""
            );

            return instance.request(err.config);
          })
          .catch(e => {
            console.error(e);

            if (errorIsUnableUoProcessRefreshToken(e)) {
              handleFail();
            } else {
              return Promise.reject(e);
            }
          });
      } else {
        handleFail();
      }
    } else {
      return Promise.reject(err);
    }
  }
);

export default instance;
