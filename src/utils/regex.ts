import { RegEx } from "./enums";

export const regexCheck = (val: string, regexString: RegEx) => {
  var re = new RegExp(regexString);
  return re.test(val);
};
