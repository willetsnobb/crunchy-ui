import React, { FC, useState, useEffect } from "react";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import queryString from "query-string";
import { useTranslation } from "react-i18next";
import {
  makeStyles,
  Theme,
  Card,
  CardHeader,
  CardContent,
  TextField,
  Button
} from "@material-ui/core";
import { setNotification } from "../../actions/commonActions";
import axios from "./../../utils/axios";
import endpoints from "./../../utils/endpoints";
import { responseIsSuccessful } from "../../utils/variousUtils";
import Page from "../../components/Page";
import { NotificationType } from "../../utils/enums";
import paths from "../../paths/paths";

const useStyles = makeStyles((theme: Theme) => ({
  wrapperPage: {
    display: "flex",
    flex: 1,
    padding: theme.spacing(3)
  },
  centeredContentWrapper: {
    margin: "auto"
  },
  cardHeader: {
    background: theme.palette.primary.main,
    color: theme.palette.common.white
  },
  formActions: {
    paddingTop: theme.spacing(2),
    display: "flex"
  },
  bottomActions: {
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    display: "flex",
    justifyContent: "space-between"
  }
}));

const connector = connect(null, { setNotification });

type ConfirmPasswordResetProps = ConnectedProps<typeof connector> &
  RouteComponentProps<{}>;

const ConfirmPasswordReset: FC<ConfirmPasswordResetProps> = ({
  setNotification,
  location: { search }
}) => {
  const classes = useStyles();
  const { t } = useTranslation();

  const parsed = queryString.parse(search);
  const code = parsed.code;

  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [reqInAction, setReqInAction] = useState(false);
  const [mismatchingPasswordsError, setMismatchingPasswordsError] = useState<
    boolean
  >(false);

  useEffect(() => {
    if (password && password2) {
      setMismatchingPasswordsError(password !== password2);
    }
  }, [password, password2]);

  const submitted = async (e: any) => {
    e.preventDefault();
    if (password && password !== password2) {
      setNotification({
        type: NotificationType.ERROR,
        message: t("passwords_mismatch")
      });
      return;
    }

    try {
      setReqInAction(true);
      const response = await axios.post(
        endpoints.POST_USER_PASSWORD_RESET_CONFIRM,
        {
          code,
          password
        }
      );
      if (responseIsSuccessful(response)) {
        setNotification({
          type: NotificationType.SUCCESS,
          message: t("password_reset")
        });
        setPassword("");
        setPassword2("");
      }
    } catch (e) {
      console.error(e);
      setNotification({
        type: NotificationType.ERROR,
        message: t("failed")
      });
    } finally {
      setReqInAction(false);
    }
  };

  return (
    <Page requestInProgress={reqInAction} className={classes.wrapperPage}>
      <div className={classes.centeredContentWrapper}>
        <Card>
          <CardHeader
            className={classes.cardHeader}
            title={t("reset_password")}
          />
          <CardContent>
            <form onSubmit={submitted}>
              <fieldset disabled={reqInAction}>
                <div>
                  <TextField
                    disabled={reqInAction}
                    autoFocus
                    id="new_password"
                    margin="dense"
                    fullWidth
                    autoComplete="new-password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    type="password"
                    label={t("create_new_password")}
                  />
                  <TextField
                    disabled={reqInAction}
                    id="repeat_new_password"
                    margin="dense"
                    fullWidth
                    autoComplete="new-password"
                    required
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    type="password"
                    label={t("repeat_password")}
                    error={mismatchingPasswordsError}
                    helperText={
                      mismatchingPasswordsError
                        ? t("passwords_must_match")
                        : null
                    }
                  />
                </div>
                <div className={classes.formActions}>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    disabled={reqInAction || mismatchingPasswordsError}
                  >
                    {t("set_new_password")}
                  </Button>
                </div>
              </fieldset>
            </form>
          </CardContent>
        </Card>
        <div className={classes.bottomActions}>
          <Button component={Link} to={paths.Login.toPath()}>
            {t("log_in")}
          </Button>
        </div>
      </div>
    </Page>
  );
};

export default withRouter(connector(ConfirmPasswordReset));
