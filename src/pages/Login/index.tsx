import React, { FC, useState, FormEvent } from "react";
import { connect, ConnectedProps } from "react-redux";
import {
  Link,
  withRouter,
  RouteComponentProps,
  Redirect
} from "react-router-dom";
import qs from "query-string";
import { useTranslation } from "react-i18next";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";
import {
  Input,
  makeStyles,
  Theme,
  withTheme,
  Typography,
  WithTheme
} from "@material-ui/core";
import Page from "../../components/Page";
import { setNotification } from "../../actions/commonActions";
import { putLSKey } from "../../utils/localstorage";
import {
  RequestStatus,
  RegEx,
  ResponseCodes,
  NotificationType,
  LSKey
} from "../../utils/enums";
import constants from "../../utils/constants";
import endpoints from "../../utils/endpoints";
import axios from "./../../utils/axios";
import {
  responseIsSuccessful,
  responseIsEmailSent,
  errorIsEmailNotSent,
  errorIsEmailNotConfirmed,
  errorIsInvalidLoginPasswordCombination
} from "../../utils/variousUtils";
import { setLoginUser } from "../../actions/loginActions";
import { regexCheck } from "../../utils/regex";
import paths from "../../paths/paths";
import { setTokens, TokensType } from "../../services/authService";
import { State } from "../../utils/types";

const useStyles = makeStyles((theme: Theme) => ({
  wrapperPage: {
    display: "flex",
    flex: 1,
    padding: theme.spacing(3)
  },
  centeredContentWrapper: {
    margin: "auto"
  },
  cardHeader: {
    background: theme.palette.primary.main,
    color: theme.palette.common.white
  },
  formActions: {
    paddingTop: theme.spacing(2),
    display: "flex"
  },
  bottomActions: {
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    display: "flex",
    justifyContent: "space-between"
  }
}));

const connector = connect(
  (state: State) => ({
    loggedIn: !!state.login.user
  }),
  {
    setLoginUser,
    setNotification
  }
);

type SearchParams = {
  bounce_to?: string;
};

type LoginProps = ConnectedProps<typeof connector> &
  WithTheme &
  RouteComponentProps<{}>;

const Login: FC<LoginProps> = ({
  loggedIn,
  setLoginUser,
  setNotification,
  history,
  location: { search },
  theme
}) => {
  const { bounce_to }: SearchParams = qs.parse(search);

  const [userLogin, setUserLogin] = useState("");
  const [password, setPassword] = useState("");
  const [lastEmail, setLastEmail] = useState("");
  const [resendRequestInProgress, setResendRequestInProgress] = useState(false);
  const [loginRequestInProgress, setLoginRequestInProgress] = useState(false);
  const [loginRequestStatus, setLoginRequestStatus] = useState<
    RequestStatus | ResponseCodes
  >(RequestStatus.NOT_REQUESTED);

  const { t } = useTranslation();
  const classes = useStyles();

  const login = async (data: any, bounce_to: any) => {
    try {
      setLoginRequestInProgress(true);

      const response = await axios.post(endpoints.POST_USER_LOGIN, data);

      if (
        responseIsSuccessful(response) &&
        response.data.data &&
        response.data.data.token &&
        response.data.data.user
      ) {
        const tokens: TokensType =
          response && response.data && response.data.data;

        setTokens(tokens);

        putLSKey(LSKey.USER, response.data.data.user);

        setLoginUser(response.data.data.user);
        setLoginRequestStatus(RequestStatus.SUCCESS);

        if (bounce_to) {
          history.push(bounce_to);
        } else {
          history.push(paths.Meals.toPath());
        }
      } else {
        setLoginRequestStatus(RequestStatus.FAIL);
        setNotification({
          message: "failed",
          type: NotificationType.ERROR
        });
      }
    } catch (e) {
      if (errorIsEmailNotConfirmed(e)) {
        setLoginRequestStatus(ResponseCodes.EMAIL_NOT_CONFIRMED);
        setNotification({
          message: "you_didnt_confirm_email",
          type: NotificationType.ERROR
        });
      } else if (errorIsInvalidLoginPasswordCombination(e)) {
        setLoginRequestStatus(ResponseCodes.INVALID_LOGIN_PASSWORD_COMBINATION);
        setNotification({
          message: "invalid_login_password_combination",
          type: NotificationType.ERROR
        });
      } else {
        setLoginRequestStatus(RequestStatus.FAIL);
        setNotification({
          message: "failed",
          type: NotificationType.ERROR
        });
      }
    } finally {
      setLoginRequestInProgress(false);
    }
  };

  const submitLogin = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    login({ password, login: userLogin }, bounce_to);
    setLastEmail(userLogin);
  };

  const resend = async () => {
    try {
      if (!regexCheck(userLogin, RegEx.EMAIL)) {
        return setNotification({
          message: "please_type_in_valid_email",
          type: NotificationType.ERROR
        });
      }

      setResendRequestInProgress(true);
      const response = await axios.post(
        endpoints.POST_RESEND_CONFIRMATION_EMAL,
        { email: userLogin }
      );
      if (responseIsEmailSent(response)) {
        setNotification({
          message: "email_sent",
          type: NotificationType.SUCCESS
        });
      } else {
        setNotification({
          message: "failed",
          type: NotificationType.ERROR
        });
      }
    } catch (e) {
      if (errorIsEmailNotSent(e)) {
        setNotification({
          message: "email_not_sent",
          type: NotificationType.ERROR
        });
      } else {
        setNotification({
          message: "failed",
          type: NotificationType.ERROR
        });
      }
    } finally {
      setResendRequestInProgress(false);
    }
  };

  if (loggedIn) {
    return <Redirect to={bounce_to || paths.Meals.toPath()} />;
  }

  return (
    <Page
      requestInProgress={loginRequestInProgress}
      className={classes.wrapperPage}
    >
      <div className={classes.centeredContentWrapper}>
        <Card>
          <CardHeader className={classes.cardHeader} title={t("log_in")} />
          <CardContent>
            <form onSubmit={submitLogin}>
              <fieldset disabled={loginRequestInProgress}>
                <div>
                  <FormControl fullWidth margin="dense">
                    <InputLabel htmlFor="login">{t("login")}</InputLabel>
                    <Input
                      autoFocus
                      id="login"
                      required
                      value={userLogin}
                      autoComplete="username"
                      type="text"
                      onChange={e => setUserLogin(e.target.value)}
                    />
                  </FormControl>

                  <FormControl fullWidth margin="dense">
                    <InputLabel htmlFor="password">{t("password")}</InputLabel>
                    <Input
                      id="password"
                      required
                      type="password"
                      autoComplete="current-password"
                      value={password}
                      onChange={e => setPassword(e.target.value)}
                    />
                  </FormControl>
                </div>
                <div className={classes.formActions}>
                  <Button
                    disabled={loginRequestInProgress}
                    type="submit"
                    variant="contained"
                    color="primary"
                  >
                    {t("log_in")}
                  </Button>
                </div>
              </fieldset>
            </form>

            {lastEmail &&
              lastEmail === userLogin &&
              loginRequestStatus === constants.EMAIL_NOT_CONFIRMED && (
                <div style={{ paddingTop: theme.spacing(2) }}>
                  <Typography gutterBottom>
                    {t(
                      "please_check_your_email_inbox_and_click_the_link_from_mail_to_confirm_registration"
                    )}
                  </Typography>
                  <Typography gutterBottom>
                    {t("didnt_recieve_email_questionmark")}
                  </Typography>
                  <div>
                    <Button disabled={resendRequestInProgress} onClick={resend}>
                      {t("send_again")}
                    </Button>
                  </div>
                </div>
              )}
          </CardContent>
        </Card>
        <div className={classes.bottomActions}>
          <Button component={Link} to={paths.Register.toPath()}>
            {t("register")}
          </Button>

          <Button component={Link} to={paths.ResetPassword.toPath()}>
            {t("reset_password")}
          </Button>
        </div>
      </div>
    </Page>
  );
};

export default withTheme(withRouter(connector(Login)));
