import React, { FC, useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { withRouter, Link, RouteComponentProps } from "react-router-dom";
import { useTranslation } from "react-i18next";
import queryString from "query-string";
import Fab from "@material-ui/core/Fab";
import {
  Card,
  Typography,
  withTheme,
  Divider,
  useMediaQuery,
  WithTheme,
  TableHead,
  TableBody,
  Table,
  TableRow,
  TableCell,
  Tooltip,
  IconButton,
  Icon,
  Paper
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import paths from "../../paths/paths";
import Page from "../../components/Page";
import LoadingSpinner from "../../components/LoadingSpinner";
import { uiNumber, uiTime, uiDateTime } from "../../utils/uiUtils";
import { Meal, State } from "../../utils/types";
import InfoCard from "../../components/InfoCard";
import MealListItem from "../../components/MealListItem";
import GridContainer from "../../components/GridContainer";
import GridItem from "../../components/GridItem";
import { Measurement, RequestStatus } from "../../utils/enums";
import { SearchParameters } from "../AddEditMeal";
import ConfirmDeleteDialog, {
  ConfirmDeleteDialogProps
} from "./../../components/ConfirmDeleteDialog";
import {
  deleteMeal,
  getMealsList,
  GetMealsType
} from "../../actions/mealsActions";

const connector = connect(
  (state: State) => ({
    useTables: state.common.useTables,
    meals: state.meals.list,
    loadingMeals: state.meals.getRequestStatus === RequestStatus.FETCHING
  }),
  { deleteMeal, getMealsList }
);

type MealsByDateProps = WithTheme &
  RouteComponentProps<{ date: string }> &
  ConnectedProps<typeof connector>;

const MealsByDate: FC<MealsByDateProps> = ({
  useTables,

  meals,
  loadingMeals: loading,
  deleteMeal,
  getMealsList,

  match: { params },
  theme
}) => {
  const { t } = useTranslation();
  const smallest = useMediaQuery(theme.breakpoints.down("xs"));

  const [totalKcal, setTotalKcal] = useState("");
  const [totalProts, setTotalProts] = useState("");
  const [totalFats, setTotalFats] = useState("");
  const [totalCarbs, setTotalCarbs] = useState("");

  type PendingDeleteState = {
    mealId?: Meal["id"];
    dialog: {
      open: ConfirmDeleteDialogProps["open"];
      title: ConfirmDeleteDialogProps["title"];
      deleteButtonDisabled: ConfirmDeleteDialogProps["deleteButtonDisabled"];
      text?: ConfirmDeleteDialogProps["text"];
    };
  };
  const [pendingDelete, setPendingDelete] = useState<PendingDeleteState>({
    dialog: { open: false, title: "", deleteButtonDisabled: false }
  });

  useEffect(() => {
    getMealsList(GetMealsType.BY_DATE, { date: params.date });
  }, [params.date, t, getMealsList]);

  useEffect(() => {
    const setTotals = (data: Meal[]) => {
      let kcal = 0;
      let prots = 0;
      let fats = 0;
      let carbs = 0;

      data.forEach((meal: Meal) => {
        kcal += Number(meal.kcal);
        prots += Number(meal.prots);
        fats += Number(meal.fats);
        carbs += Number(meal.carbs);
      });

      const total = prots + fats + carbs;

      let protsPercent = 0;
      let fatsPercent = 0;
      let carbsPercent = 0;

      if (total > 0) {
        protsPercent = uiNumber((prots / total) * 100);
        fatsPercent = uiNumber((fats / total) * 100);
        carbsPercent = uiNumber(100 - fatsPercent - protsPercent);
      }

      setTotalKcal(`${uiNumber(kcal)}`);
      setTotalProts(
        `${uiNumber(prots)}${t("grams_acronym")}${
          total > 0 ? ` (${protsPercent}%)` : ""
        }`
      );
      setTotalFats(
        `${uiNumber(fats)}${t("grams_acronym")}${
          total > 0 ? ` (${fatsPercent}%)` : ""
        }`
      );
      setTotalCarbs(
        `${uiNumber(carbs)}${t("grams_acronym")}${
          total > 0 ? ` (${carbsPercent}%)` : ""
        }`
      );
    };

    setTotals(meals);
  }, [meals, t]);

  if (!params.date) {
    return <p>specify date please</p>;
  }

  const uiAmountUnitFromMeal = (meal: Meal) => {
    if (meal.measurement === Measurement["100_GRAM"]) {
      return t("grams");
    } else if (meal.measurement === Measurement["1_PORTION"]) {
      return t("portions");
    }
  };

  return (
    <Page style={{ padding: theme.spacing(3) }}>
      <ConfirmDeleteDialog
        open={pendingDelete.dialog.open}
        title={pendingDelete.dialog.title}
        text={pendingDelete.dialog.text}
        deleteButtonDisabled={pendingDelete.dialog.deleteButtonDisabled}
        onClose={() => {
          setPendingDelete(old => ({
            ...old,
            dialog: { ...old.dialog, open: false }
          }));
        }}
        onConfirmDelete={() => {
          if (pendingDelete.mealId) {
            setPendingDelete(old => ({
              ...old,
              dialog: { ...old.dialog, deleteButtonDisabled: true }
            }));
            deleteMeal(pendingDelete.mealId, false);
          }
        }}
      />

      {loading && <LoadingSpinner />}

      {!loading && (!meals || meals.length === 0) && (
        <Typography>{t("no_meals_found")}</Typography>
      )}

      {!loading && meals && meals.length > 0 && (
        <>
          {smallest && (
            <Card
              style={{
                background: theme.palette.primary.main,
                color: theme.palette.common.white,
                padding: theme.spacing(3)
              }}
            >
              <Typography gutterBottom>
                {`${t("kcal")}: ${totalKcal}`}
              </Typography>
              <Typography gutterBottom>
                {`${t("prots")}: ${totalProts}`}
              </Typography>
              <Typography gutterBottom>
                {`${t("fats")}: ${totalFats}`}
              </Typography>
              <Typography gutterBottom>
                {`${t("carbs")}: ${totalCarbs}`}
              </Typography>
            </Card>
          )}

          {!smallest && (
            <GridContainer>
              <GridItem>
                <InfoCard title={t("kcal")} value={totalKcal} />
              </GridItem>
              <GridItem>
                <InfoCard title={t("prots")} value={totalProts} />
              </GridItem>
              <GridItem>
                <InfoCard title={t("fats")} value={totalFats} />
              </GridItem>
              <GridItem>
                <InfoCard title={t("carbs")} value={totalCarbs} />
              </GridItem>
            </GridContainer>
          )}

          <Divider
            style={{
              marginTop: theme.spacing(3),
              marginBottom: theme.spacing(3)
            }}
          />

          {!useTables && (
            <GridContainer>
              {meals.map((meal: Meal) => (
                <GridItem key={meal.id}>
                  <MealListItem item={meal} />
                </GridItem>
              ))}
            </GridContainer>
          )}

          {useTables && (
            <Paper style={{ overflow: "hidden" }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>{t("actions")}</TableCell>
                    <TableCell>{t("time")}</TableCell>
                    <TableCell>{t("food")}</TableCell>
                    <TableCell>{t("amount")}</TableCell>
                    <TableCell>{t("kcal")}</TableCell>
                    <TableCell>{t("prots")}</TableCell>
                    <TableCell>{t("fats")}</TableCell>
                    <TableCell>{t("carbs")}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {meals.map((meal: Meal) => {
                    const repeatMealSearchParams: SearchParameters = {
                      preselected_food_id: `${meal.food_id}`,
                      preselected_amount_expression: `${meal.amount_expression ||
                        meal.amount}`
                    };

                    return (
                      <TableRow key={meal.id}>
                        <TableCell
                          style={{
                            paddingTop: 0,
                            paddingBottom: 0,
                            width: 176
                          }}
                        >
                          <Tooltip title={t("edit")}>
                            <IconButton
                              component={Link}
                              to={paths.EditMeal.toPath({
                                mealId: meal.id
                              })}
                            >
                              <Icon>edit</Icon>
                            </IconButton>
                          </Tooltip>
                          <Tooltip title={t("repeat")}>
                            <IconButton
                              to={`${paths.AddMeal.toPath({
                                mealId: meal.id
                              })}?${queryString.stringify(
                                repeatMealSearchParams
                              )}`}
                              component={Link}
                            >
                              <Icon>autorenew</Icon>
                            </IconButton>
                          </Tooltip>
                          <Tooltip title={t("delete")}>
                            <IconButton
                              disabled={!!meal.deleting}
                              onClick={e => {
                                setPendingDelete({
                                  mealId: meal.id,
                                  dialog: {
                                    open: true,
                                    deleteButtonDisabled: false,
                                    title: t(
                                      "do_you_want_to_delete_the_meal_qm"
                                    ),
                                    text:
                                      t("the_following_meal_will_be_deleted") +
                                      ": " +
                                      meal.name +
                                      ", " +
                                      uiNumber(meal.amount) +
                                      " " +
                                      uiAmountUnitFromMeal(meal) +
                                      " (" +
                                      uiDateTime(meal.timestamp) +
                                      ")"
                                  }
                                });
                              }}
                            >
                              <Icon>delete</Icon>
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                        <TableCell>{uiTime(meal.timestamp)}</TableCell>
                        <TableCell>{meal.name}</TableCell>
                        <TableCell>{`${uiNumber(
                          meal.amount
                        )} ${uiAmountUnitFromMeal(meal)}`}</TableCell>
                        <TableCell>{uiNumber(meal.kcal)}</TableCell>
                        <TableCell>
                          {uiNumber(meal.prots)}
                          {t("grams_acronym")}
                        </TableCell>
                        <TableCell>
                          {uiNumber(meal.fats)}
                          {t("grams_acronym")}
                        </TableCell>
                        <TableCell>
                          {uiNumber(meal.carbs)}
                          {t("grams_acronym")}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </Paper>
          )}
        </>
      )}

      <Fab
        style={{
          position: "fixed",
          bottom: theme.spacing(3),
          right: theme.spacing(3)
        }}
        component={Link}
        to={`${paths.AddMeal.toPath()}?${queryString.stringify({
          date: params.date
        })}`}
        color="secondary"
        aria-label={t("add_meal")}
      >
        <AddIcon />
      </Fab>
    </Page>
  );
};

export default withTheme(withRouter(connector(MealsByDate)));
