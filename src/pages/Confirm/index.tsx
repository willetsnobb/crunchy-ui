import React, { FC, useEffect, useState } from "react";
import { withRouter, Redirect, RouteComponentProps } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import queryString from "query-string";
import axios from "../../utils/axios";
import endpoints from "../../utils/endpoints";
import { responseIsSuccessful } from "../../utils/variousUtils";
import Page from "../../components/Page";
import { setNotification } from "../../actions/commonActions";
import paths from "../../paths/paths";
import { NotificationType } from "../../utils/enums";
import FullPageLoader from "../../components/FullPageLoader";

const connector = connect(null, { setNotification });

type SearchParams = {
  code?: string;
};

type ConfirmProps = ConnectedProps<typeof connector> & RouteComponentProps<{}>;

const Confirm: FC<ConfirmProps> = ({
  setNotification,
  location: { search }
}) => {
  const parsed: SearchParams = queryString.parse(search);
  const { code } = parsed;

  const [confirmed, setConfirmed] = useState(false);
  const [confirmRequestInProgress, setConfirmRequestInProgress] = useState(
    false
  );

  useEffect(() => {
    const confirm = async (code: string) => {
      try {
        setConfirmRequestInProgress(true);

        const response = await axios.post(endpoints.POST_USER_CONFIRM, {
          code
        });

        if (responseIsSuccessful(response)) {
          setNotification({
            type: NotificationType.SUCCESS,
            message: "confirmed"
          });

          setConfirmed(true);
        } else {
          setNotification({
            type: NotificationType.ERROR,
            message: "failed"
          });
        }
      } catch (e) {
        console.error(e);
        setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        });
      } finally {
        setConfirmRequestInProgress(false);
      }
    };
    if (code) {
      confirm(code);
    }
  }, [code, setConfirmRequestInProgress, setNotification]);

  if (confirmed && !confirmRequestInProgress) {
    return <Redirect to={paths.Login.toPath()} />;
  }

  return <Page>{confirmRequestInProgress && <FullPageLoader />}</Page>;
};

export default withRouter(connector(Confirm));
