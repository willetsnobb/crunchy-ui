import React, { FC } from "react";
import { Link as RouterLink } from "react-router-dom";
import { withTheme, WithTheme, Typography, Button } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import Page from "../../components/Page";
import paths from "../../paths/paths";

type NotFoundProps = WithTheme;

const NotFound: FC<NotFoundProps> = ({ theme }) => {
  const { t } = useTranslation();

  return (
    <Page
      style={{
        flex: 1,
        display: "flex",
        padding: theme.spacing(3),
        color: theme.palette.text.primary
      }}
    >
      <div style={{ margin: "auto", textAlign: "center" }}>
        <Typography variant={"h3"} gutterBottom>
          404: {t("page_not_found")}
        </Typography>
        <Typography>
          <Button component={RouterLink} to={paths.Main.toPath()}>
            {t("visit_main_page")}
          </Button>
        </Typography>
      </div>
    </Page>
  );
};

export default withTheme(NotFound);
