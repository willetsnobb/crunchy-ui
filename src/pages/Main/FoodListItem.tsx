import React, { FC } from "react";
import {
  makeStyles,
  Card,
  CardContent,
  CardActions,
  Button,
  Typography,
  Table,
  TableBody,
  TableRow,
  TableCell
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import paths from "../../paths/paths";
import { uiNumber } from "../../utils/uiUtils";
import { Food } from "../../utils/types";

const useStyles = makeStyles(theme => ({
  impressions: {
    paddingTop: theme.spacing(3)
  }
}));

type FoodListItemProps = {
  food: Food;
}

const FoodListItem: FC<FoodListItemProps> = ({ food }) => {
  const [t] = useTranslation();

  const classes = useStyles();

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="h2">
          {food.name}
        </Typography>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>{t("measured_by")}</TableCell>
              <TableCell>{t(`measurements.${food.measurement}`)}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>{t("kcal")}</TableCell>
              <TableCell>{uiNumber(food.kcal)}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>{t("prots")}</TableCell>
              <TableCell>
                {uiNumber(food.prots)} {t("grams_acronym")}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>{t("fats")}</TableCell>
              <TableCell>
                {uiNumber(food.fats)} {t("grams_acronym")}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>{t("carbs")}</TableCell>
              <TableCell>
                {uiNumber(food.carbs)} {t("grams_acronym")}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
        {(Number(food.likes) > 0 || Number(food.dislikes) > 0) && (
          <Typography className={classes.impressions} variant="body2">
            {food.likes} {t("likes")}, {food.dislikes} {t("dislikes_lc")}
          </Typography>
        )}
      </CardContent>
      <CardActions>
        <Button
          component={Link}
          to={paths.ViewPublicFood.toPath({ foodId: food.id })}
        >
          {t("view")}
        </Button>
      </CardActions>
    </Card>
  );
};

export default FoodListItem;
