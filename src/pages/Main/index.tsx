import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { makeStyles, Typography } from "@material-ui/core";
import Page from "../../components/Page";
import { responseIsSuccessful } from "../../utils/variousUtils";
import FoodListItem from "./FoodListItem";
import { Food } from "../../utils/types";
import GridContainer from "../../components/GridContainer";
import GridItem from "../../components/GridItem";
import FullPageLoader from "../../components/FullPageLoader";
import foodService from "../../services/foodService";

const useStyles = makeStyles(theme => ({
  wrapper: {
    padding: theme.spacing(3),
    color: theme.palette.text.primary
  },
  pageName: { marginBottom: theme.spacing(3) }
}));

const Main = () => {
  const [t] = useTranslation();

  const classes = useStyles();

  const [foods, setFoods] = useState([]);
  const [loadingFoods, setLoadingFoods] = useState(false);

  useEffect(() => {
    const getFoods = async () => {
      try {
        setLoadingFoods(true);

        const response = await foodService.getAllPublic();

        if (responseIsSuccessful(response)) {
          setFoods(response.data.data);
        }
      } catch (e) {
        console.error(e);
      } finally {
        setLoadingFoods(false);
      }
    };

    getFoods();
  }, []);

  return (
    <Page className={classes.wrapper}>
      {loadingFoods && <FullPageLoader />}

      {!loadingFoods && (
        <>
          <Typography className={classes.pageName} variant="h6" component="h1">
            {t("public_foods")}
          </Typography>

          {(!foods || foods.length === 0) && (
            <Typography>{t("no_items_found")}</Typography>
          )}

          {foods && foods.length > 0 && (
            <GridContainer>
              {foods.map((food: Food) => (
                <GridItem key={food.id}>
                  <FoodListItem food={food} />
                </GridItem>
              ))}
            </GridContainer>
          )}
        </>
      )}
    </Page>
  );
};

export default Main;
