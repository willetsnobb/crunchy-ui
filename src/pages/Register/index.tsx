import React, { useState, FC, useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { useTranslation } from "react-i18next";
import {
  Input,
  Button,
  Card,
  CardHeader,
  CardContent,
  FormControl,
  InputLabel,
  makeStyles,
  Theme,
  Typography,
  withTheme,
  FormHelperText,
  WithTheme
} from "@material-ui/core";

import { setNotification } from "../../actions/commonActions";
import constants from "../../utils/constants";
import { register } from "../../actions/registerActions";
import axios from "./../../utils/axios";
import endpoints from "../../utils/endpoints";
import {
  responseIsEmailSent,
  errorIsEmailNotSent
} from "../../utils/variousUtils";
import { regexCheck } from "../../utils/regex";
import Page from "../../components/Page";
import { RequestStatus, RegEx, NotificationType } from "../../utils/enums";
import { State } from "../../utils/types";

const useStyles = makeStyles((theme: Theme) => ({
  wrapperPage: {
    display: "flex",
    flex: 1,
    padding: theme.spacing(3)
  },
  cardHeader: {
    background: theme.palette.primary.main,
    color: theme.palette.common.white
  },
  mainCard: {
    margin: "auto"
  },
  formActions: {
    paddingTop: theme.spacing(2),
    display: "flex"
  }
}));

const connector = connect(
  (state: State) => ({
    registerRequestInProgress:
      state.register.requestStatus === RequestStatus.FETCHING,
    registerRequestStatus: state.register.requestStatus
  }),
  { register, setNotification }
);

type RegisterProps = ConnectedProps<typeof connector> & WithTheme;

const Register: FC<RegisterProps> = ({
  registerRequestInProgress,
  registerRequestStatus,
  register,
  setNotification,
  theme
}) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const [registerEmail, setRegisterEmail] = useState<string>("");
  const [resendRequestInProgress, setResendRequestInProgress] = useState<
    boolean
  >(false);
  const [login, setLogin] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [password2, setPassword2] = useState<string>("");

  const [mismatchingPasswordsError, setMismatchingPasswordsError] = useState<
    boolean
  >(false);
  const [badEmailFormatError, setBadEmailFormatError] = useState<boolean>(
    false
  );

  useEffect(() => {
    if (login) {
      setBadEmailFormatError(!regexCheck(login, RegEx.EMAIL));
    }
  }, [login]);

  useEffect(() => {
    if (password && password2) {
      setMismatchingPasswordsError(password !== password2);
    }
  }, [password, password2]);

  const submitRegister = (e: any) => {
    e.preventDefault();
    if (password !== password2) {
      return setNotification({
        type: NotificationType.ERROR,
        message: "passwords_mismatch"
      });
    }

    if (!regexCheck(login, RegEx.EMAIL)) {
      return setNotification({
        type: NotificationType.ERROR,
        message: "please_type_in_valid_email"
      });
    }

    register({ password, login }, () => {
      setLogin("");
      setPassword("");
      setPassword2("");
    });

    setRegisterEmail(login);
  };

  const resent = async () => {
    try {
      if (!regexCheck(login, RegEx.EMAIL)) {
        return setNotification({
          type: NotificationType.ERROR,
          message: "please_type_in_valid_email"
        });
      }

      setResendRequestInProgress(true);

      const response = await axios.post(
        endpoints.POST_RESEND_CONFIRMATION_EMAL,
        { email: login }
      );
      if (responseIsEmailSent(response)) {
        setNotification({
          type: NotificationType.SUCCESS,
          message: "email_sent"
        });
      } else {
        setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        });
      }
    } catch (e) {
      if (errorIsEmailNotSent(e)) {
        setNotification({
          type: NotificationType.ERROR,
          message: "email_not_sent"
        });
      } else {
        setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        });
      }
    } finally {
      setResendRequestInProgress(false);
    }
  };

  return (
    <Page
      className={classes.wrapperPage}
      requestInProgress={registerRequestInProgress}
    >
      <Card className={classes.mainCard}>
        <CardHeader className={classes.cardHeader} title={t("registration")} />
        <CardContent>
          <form onSubmit={submitRegister}>
            <fieldset disabled={registerRequestInProgress}>
              <div>
                <FormControl margin="dense" fullWidth>
                  <InputLabel htmlFor="login">{t("email")}</InputLabel>
                  <Input
                    autoFocus
                    disabled={registerRequestInProgress}
                    id="login"
                    required
                    error={badEmailFormatError}
                    type="email"
                    autoComplete="email"
                    value={login}
                    onChange={(e: any) => setLogin(e.target.value)}
                  />
                  {badEmailFormatError && (
                    <FormHelperText>
                      {t("please_type_in_valid_email")}
                    </FormHelperText>
                  )}
                </FormControl>

                <FormControl margin="dense" fullWidth>
                  <InputLabel htmlFor="password">{t("password")}</InputLabel>
                  <Input
                    disabled={registerRequestInProgress}
                    id="password"
                    required
                    type="password"
                    value={password}
                    autoComplete="new-password"
                    onChange={(e: any) => setPassword(e.target.value)}
                  />
                </FormControl>

                <FormControl margin="dense" fullWidth>
                  <InputLabel htmlFor="repeat-password">
                    {t("repeat_password")}
                  </InputLabel>
                  <Input
                    disabled={registerRequestInProgress}
                    required
                    error={mismatchingPasswordsError}
                    id="repeat-password"
                    type="password"
                    value={password2}
                    autoComplete="new-password"
                    onChange={(e: any) => setPassword2(e.target.value)}
                  />
                  {mismatchingPasswordsError && (
                    <FormHelperText>{t("passwords_must_match")}</FormHelperText>
                  )}
                </FormControl>
              </div>

              <div className={classes.formActions}>
                <Button
                  disabled={
                    badEmailFormatError ||
                    mismatchingPasswordsError ||
                    registerRequestInProgress
                  }
                  type="submit"
                  variant="contained"
                  color="primary"
                >
                  {t("register")}
                </Button>
                <div style={{ display: "flex", paddingLeft: theme.spacing(2) }}>
                  <Typography style={{ margin: "auto" }}>
                    {t("we_will_send_you_email")}
                  </Typography>
                </div>
              </div>
            </fieldset>
          </form>

          {registerEmail === login && login && (
            <div>
              {registerRequestStatus === constants.EMAIL_SENT && (
                <Typography style={{ paddingTop: theme.spacing(2) }}>
                  {t("we_sent_you_email_with_link_to_confirm_registration")}
                </Typography>
              )}

              {registerRequestStatus === constants.EMAIL_NOT_SENT && (
                <Typography style={{ paddingTop: theme.spacing(2) }}>
                  {t("error_occured_while_sending_email")}
                </Typography>
              )}

              {(registerRequestStatus === constants.EMAIL_NOT_SENT ||
                registerRequestStatus === constants.EMAIL_SENT) && (
                <>
                  <Typography
                    gutterBottom
                    style={{ paddingTop: theme.spacing(2) }}
                  >
                    {t("didnt_recieve_email_questionmark")}
                  </Typography>
                  <div>
                    <Button disabled={resendRequestInProgress} onClick={resent}>
                      {t("send_again")}
                    </Button>
                  </div>
                </>
              )}
            </div>
          )}
        </CardContent>
      </Card>
    </Page>
  );
};

export default withTheme(connector(Register));
