import React, { useState, FC, useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import {
  makeStyles,
  Theme,
  Card,
  TextField,
  Button,
  withTheme,
  Typography,
  CardHeader,
  CardContent,
  WithTheme
} from "@material-ui/core";
import axios from "../../utils/axios";
import endpoints from "../../utils/endpoints";
import {
  responseIsSuccessful,
  responseIsEmailSent,
  errorIsEmailNotSent
} from "../../utils/variousUtils";
import Page from "../../components/Page";
import { NotificationType, RegEx } from "../../utils/enums";
import { setNotification } from "../../actions/commonActions";
import { Notification } from "../../utils/types";
import paths from "../../paths/paths";
import { regexCheck } from "../../utils/regex";

const useStyles = makeStyles((theme: Theme) => ({
  wrapperPage: {
    display: "flex",
    flex: 1,
    padding: theme.spacing(3)
  },
  centeredContentWrapper: {
    margin: "auto"
  },
  cardHeader: {
    background: theme.palette.primary.main,
    color: theme.palette.common.white
  },
  formActions: {
    paddingTop: theme.spacing(2),
    display: "flex"
  },
  bottomActions: {
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    display: "flex",
    justifyContent: "space-between"
  }
}));

const connector = connect(null, { setNotification });

type ResetPasswordProps = WithTheme & ConnectedProps<typeof connector>;

const ResetPassword: FC<ResetPasswordProps> = ({ setNotification, theme }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  const [email, setEmail] = useState("");
  const [requestInAction, setRequestInAction] = useState(false);

  const [badEmailFormatError, setBadEmailFormatError] = useState<boolean>(
    false
  );

  useEffect(() => {
    if (email) {
      setBadEmailFormatError(!regexCheck(email, RegEx.EMAIL));
    }
  }, [email]);

  const submitted = async (e: any) => {
    try {
      e.preventDefault();

      setRequestInAction(true);

      const response = await axios.post(
        endpoints.POST_USER_PASSWORD_RESET_REQUEST,
        {
          email
        }
      );

      if (responseIsSuccessful(response) && responseIsEmailSent(response)) {
        const notification: Notification = {
          type: NotificationType.SUCCESS,
          message: "email_sent"
        };

        setNotification(notification);

        setEmail("");
      }
    } catch (e) {
      console.error(e);
      if (errorIsEmailNotSent(e)) {
        const notification: Notification = {
          type: NotificationType.ERROR,
          message: "email_not_sent"
        };

        setNotification(notification);
      } else {
        const notification: Notification = {
          type: NotificationType.ERROR,
          message: "failed"
        };

        setNotification(notification);
      }
    } finally {
      setRequestInAction(false);
    }
  };

  return (
    <Page requestInProgress={requestInAction} className={classes.wrapperPage}>
      <div className={classes.centeredContentWrapper}>
        <Card>
          <CardHeader
            className={classes.cardHeader}
            title={t("reset_password")}
          />
          <CardContent>
            <form onSubmit={submitted}>
              <fieldset disabled={requestInAction}>
                <div>
                  <TextField
                    id="email"
                    autoFocus
                    fullWidth
                    autoComplete="email"
                    error={badEmailFormatError}
                    helperText={
                      badEmailFormatError
                        ? t("please_type_in_valid_email")
                        : null
                    }
                    required
                    label={t("email")}
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                  />
                </div>
                <div className={classes.formActions}>
                  <div>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      disabled={requestInAction || badEmailFormatError}
                    >
                      {t("reset_password")}
                    </Button>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flex: 1,
                      paddingLeft: theme.spacing(2)
                    }}
                  >
                    <Typography style={{ margin: "auto" }}>
                      {t("we_will_send_you_a_password_reset_link")}
                    </Typography>
                  </div>
                </div>
              </fieldset>
            </form>
          </CardContent>
        </Card>
        <div className={classes.bottomActions}>
          <Button component={Link} to={paths.Login.toPath()}>
            {t("log_in")}
          </Button>
        </div>
      </div>
    </Page>
  );
};

export default withTheme(connector(ResetPassword));
