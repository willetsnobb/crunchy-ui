import React, { FC, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import {
  Typography,
  Table,
  TableBody,
  TableRow,
  TableCell,
  makeStyles
} from "@material-ui/core";
import Page from "../../components/Page";
import Loading from "../../components/LoadingSpinner";
import { responseIsSuccessful } from "../../utils/variousUtils";
import { uiNumber } from "../../utils/uiUtils";
import { Food } from "../../utils/types";
import { RouteComponentProps } from "react-router-dom";
import foodService from "../../services/foodService";

const useStyles = makeStyles(theme => ({
  wrapper: {
    color: theme.palette.text.primary,
    padding: theme.spacing(3)
  },
  pageName: { marginBottom: theme.spacing(3) },
  about: { marginTop: theme.spacing(3) }
}));

type ViewFoodProps = RouteComponentProps<{ foodId: string }>;

const ViewFood: FC<ViewFoodProps> = ({
  match: {
    params: { foodId }
  }
}) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const [loading, setLoading] = useState<boolean>(false);
  const [foodItem, setFoodItem] = useState<Food | null>(null);

  useEffect(() => {
    function getFoodItem(id: number | string) {
      setLoading(true);
      foodService
        .getPublic(id)
        .then(
          response => {
            if (responseIsSuccessful(response)) {
              const item = response && response.data && response.data.data;

              if (item) {
                setFoodItem(item);
              }
            }
          },
          e => {
            console.error(e);
          }
        )
        .catch(e => {
          console.error(e);
        })
        .finally(() => {
          setLoading(false);
        });
    }

    if (foodId) {
      getFoodItem(foodId);
    }
  }, [foodId]);

  if (!foodItem) {
    return null;
  }

  return (
    <Page className={classes.wrapper}>
      {loading && <Loading />}

      {!loading && (
        <>
          <Typography className={classes.pageName} variant="h6" component="h1">
            {`${foodItem.name} ${t("nutrition_value")}`}
          </Typography>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell>{t("measured_by")}</TableCell>
                <TableCell>
                  {t(`measurements.${foodItem.measurement}`)}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>{t("kcal")}</TableCell>
                <TableCell>{uiNumber(foodItem.kcal)}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>{t("prots")}</TableCell>
                <TableCell>
                  {uiNumber(foodItem.prots)} {t("grams_acronym")}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>{t("fats")}</TableCell>
                <TableCell>
                  {uiNumber(foodItem.fats)} {t("grams_acronym")}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>{t("carbs")}</TableCell>
                <TableCell>
                  {uiNumber(foodItem.carbs)} {t("grams_acronym")}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
          <Typography className={classes.about}>{foodItem.about}</Typography>
        </>
      )}
    </Page>
  );
};

export default ViewFood;
