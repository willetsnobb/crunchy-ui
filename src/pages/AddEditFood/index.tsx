import React, { useState, useEffect, FC, FormEvent } from "react";
import { connect, ConnectedProps } from "react-redux";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { evaluate } from "mathjs";
import {
  Checkbox,
  FormControlLabel,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Icon,
  TextField,
  withTheme,
  Typography,
  IconButton,
  makeStyles,
  Theme,
  WithTheme
} from "@material-ui/core";
import { postFood, updateFood } from "../../actions/foodsActions";
import axios from "../../utils/axios";
import measurings from "../../utils/constArrays/measurings";
import endpoints from "../../utils/endpoints";
import { responseIsSuccessful } from "../../utils/variousUtils";
import Page from "../../components/Page";
import { setNotification } from "../../actions/commonActions";
import paths from "../../paths/paths";
import { Measurement, NotificationType } from "../../utils/enums";
import { Food, State } from "../../utils/types";
import { uiNumber } from "../../utils/uiUtils";
import FullPageLoader from "../../components/FullPageLoader";
import mealService from "../../services/mealService";
import foodService from "../../services/foodService";

const useStyles = makeStyles((theme: Theme) => ({
  checkboxLabel: {
    color: theme.palette.text.primary
  }
}));

const mapStateToProps = (state: State) => ({
  requestInProgress: state.foods.creating || state.foods.updating
});

const connectActions = { postFood, updateFood, setNotification };

const connector = connect(mapStateToProps, connectActions);

type AddEditFoodProps = ConnectedProps<typeof connector> &
  WithTheme &
  RouteComponentProps<{ foodId: string }>;

type NutritionFormulasState = {
  kcal: string;
  prots: string;
  fats: string;
  carbs: string;
};

type NutritionState = {
  kcal: number;
  prots: number;
  fats: number;
  carbs: number;
};

const AddEditFood: FC<AddEditFoodProps> = ({
  requestInProgress,
  postFood,
  updateFood,
  setNotification,
  history,
  match: {
    params: { foodId }
  },
  theme
}) => {
  const classes = useStyles();

  const [formulas, setFormulas] = useState<NutritionFormulasState>({
    kcal: "",
    prots: "",
    fats: "",
    carbs: ""
  });
  const [nutrition, setNutrition] = useState<NutritionState>({
    kcal: 0,
    prots: 0,
    fats: 0,
    carbs: 0
  });

  const [localRequestInProgress, setLocalRequestInProgress] = useState<boolean>(
    false
  );

  const [loading, setLoading] = useState<boolean>(false);

  const [initialData, setInitialData] = useState<Food | null>(null);
  const [isEditingMode, setIsEditingMode] = useState<boolean>(false);
  const [measurement, setMeasurement] = useState<Measurement>(
    Measurement["100_GRAM"]
  );
  const [about, setAbout] = useState("");
  const [name, setName] = useState("");
  const [isPublic, setIsPublic] = useState(false);
  const [canBePublic, setCanBePublic] = useState(true);
  const [warnOfMeals, setWarnOfMeals] = useState(false);
  const [mealsWarnAmount, setMealsWarnAmount] = useState(0);
  const [excessNutriantsError, setExcessNutriantsError] = useState(false);

  const { t } = useTranslation();

  const uiText = (t1: string, t2: string, t3?: string) => {
    if (t1 && t2 && t3) {
      return `${t1} ${t("in")} ${t2} (${t3})`;
    } else if (t1 && t3) {
      return `${t1} (${t3})`;
    } else if (t1 && t2) {
      return `${t1} ${t("in")} ${t2}`;
    } else if (t1) {
      return t1;
    } else {
      return "whops... something went wrong";
    }
  };

  const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (measurement && measurings.some(m => m === measurement)) {
      const newFood = {
        public: isPublic,
        kcal: nutrition.kcal,
        prots: nutrition.prots,
        fats: nutrition.fats,
        carbs: nutrition.carbs,
        kcal_expression: formulas.kcal,
        prots_expression: formulas.prots,
        fats_expression: formulas.fats,
        carbs_expression: formulas.carbs,
        measurement,
        about,
        name
      };

      if (isEditingMode) {
        const updatedItem: Food = {
          ...initialData,
          ...newFood
        };

        if ("can_be_public" in updatedItem) {
          delete updatedItem.can_be_public;
        }

        updateFood(updatedItem);
      } else {
        await postFood(
          newFood,
          /* onSuccess = */ () => {
            setName("");

            setFormulas({
              kcal: "",
              prots: "",
              fats: "",
              carbs: ""
            });

            setNutrition({
              kcal: 0,
              prots: 0,
              fats: 0,
              carbs: 0
            });

            setAbout("");
          }
        );
      }
    } else {
      setNotification({
        type: NotificationType.ERROR,
        message: "please_specify_measurement"
      });
    }
  };

  const processFormula = (input: string): number => {
    const preparedInput = input.trim().replace(",", ".");

    if (preparedInput === "") {
      return 0;
    }

    try {
      return evaluate(preparedInput);
    } catch {
      return 0;
    }
  };

  useEffect(() => {
    const newValue = processFormula(formulas.carbs);
    setNutrition(old => ({ ...old, carbs: newValue }));
  }, [formulas.carbs]);

  useEffect(() => {
    const newValue = processFormula(formulas.fats);
    setNutrition(old => ({ ...old, fats: newValue }));
  }, [formulas.fats]);

  useEffect(() => {
    const newValue = processFormula(formulas.prots);
    setNutrition(old => ({ ...old, prots: newValue }));
  }, [formulas.prots]);

  useEffect(() => {
    const newValue = processFormula(formulas.kcal);
    setNutrition(old => ({ ...old, kcal: newValue }));
  }, [formulas.kcal]);

  useEffect(() => {
    setExcessNutriantsError(
      measurement === Measurement["100_GRAM"] &&
        nutrition.prots + nutrition.fats + nutrition.carbs > 100
    );
  }, [measurement, nutrition]);

  useEffect(() => {
    const fetchFood = async (id: string) => {
      try {
        setLoading(true);
        const response = await foodService.get(id);

        if (responseIsSuccessful(response)) {
          const fetchedFoodItem: Food =
            response && response.data && response.data.data;

          setInitialData(fetchedFoodItem);

          setName(fetchedFoodItem.name);

          setMeasurement(fetchedFoodItem.measurement);

          if (
            fetchedFoodItem.kcal_expression ||
            fetchedFoodItem.prots_expression ||
            fetchedFoodItem.fats_expression ||
            fetchedFoodItem.carbs_expression
          ) {
            setFormulas({
              kcal: fetchedFoodItem.kcal_expression || "",
              prots: fetchedFoodItem.prots_expression || "",
              fats: fetchedFoodItem.fats_expression || "",
              carbs: fetchedFoodItem.carbs_expression || ""
            });
          } else {
            setFormulas({
              kcal: fetchedFoodItem.kcal + "",
              prots: fetchedFoodItem.prots + "",
              fats: fetchedFoodItem.fats + "",
              carbs: fetchedFoodItem.carbs + ""
            });
          }

          setAbout(fetchedFoodItem.about);

          setCanBePublic(!!fetchedFoodItem.can_be_public);
          setIsPublic(fetchedFoodItem.public);
        }
      } catch (e) {
        console.error(e);
        setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        });
      } finally {
        setLoading(false);
      }
    };

    if (foodId) {
      setIsEditingMode(true);
      fetchFood(foodId);
    } else {
      setIsEditingMode(false);
    }
  }, [foodId, setNotification]);

  const deleteFoodById = async () => {
    try {
      setLocalRequestInProgress(true);

      const response = await foodService.delete(foodId);
      if (responseIsSuccessful(response)) {
        setNotification({
          type: NotificationType.SUCCESS,
          message: "deleted"
        });

        history.push(paths.Foods.toPath());
      } else {
        setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        });
      }
    } catch (e) {
      console.error(e);
      setNotification({
        type: NotificationType.ERROR,
        message: "failed"
      });
    } finally {
      setLocalRequestInProgress(false);
    }
  };

  const deleteMealsAndFood = async () => {
    try {
      setLocalRequestInProgress(true);

      const response = await axios.delete(endpoints.DELETE_MEALS_BY_FOOD, {
        data: { food_id: foodId }
      });
      if (responseIsSuccessful(response)) {
        await deleteFoodById();
      } else {
        setNotification({
          type: NotificationType.ERROR,
          message: "failed"
        });
      }
    } catch (e) {
      console.error(e);
      setNotification({
        type: NotificationType.ERROR,
        message: "failed"
      });
    } finally {
      setLocalRequestInProgress(false);
    }
  };

  const attemptToDeleteFood = async () => {
    try {
      setLocalRequestInProgress(true);

      const response = await mealService.getByFood(foodId);

      if (responseIsSuccessful(response)) {
        if (response.data.data && response.data.data.length > 0) {
          setWarnOfMeals(true);
          setMealsWarnAmount(response.data.data.length);
        } else if (response.data.data && response.data.data.length === 0) {
          await deleteFoodById();
        }
      }
    } catch (e) {
      console.error(e);
      setNotification({
        type: NotificationType.ERROR,
        message: "failed"
      });
    } finally {
      setLocalRequestInProgress(false);
    }
  };

  useEffect(() => {
    if (canBePublic === false) {
      setIsPublic(false);
    }
  }, [canBePublic]);

  return (
    <Page
      requestInProgress={requestInProgress || localRequestInProgress}
      topAppBarIcons={
        isEditingMode && (
          <IconButton
            disabled={
              requestInProgress || localRequestInProgress || warnOfMeals
            }
            aria-label={t("delete")}
            color="inherit"
            onClick={() => {
              attemptToDeleteFood();
            }}
            style={{ cursor: "pointer" }}
          >
            <Icon>delete</Icon>
          </IconButton>
        )
      }
      style={{
        padding: theme.spacing(3)
      }}
    >
      {loading && <FullPageLoader />}

      {!loading && (
        <>
          {warnOfMeals && (
            <div
              style={{
                padding: theme.spacing(1),
                marginBottom: theme.spacing(2),
                borderRadius: 5,
                background: theme.palette.error.main,
                color: theme.palette.common.white
              }}
            >
              <Typography gutterBottom>
                {`${t("this_food_has")} ${mealsWarnAmount} ${t(
                  "associated_meals_lc"
                )}. ${t("food_and_associated_meals_will_be_deleted")}`}
              </Typography>
              <div>
                <Button
                  disabled={requestInProgress || localRequestInProgress}
                  onClick={deleteMealsAndFood}
                  style={{
                    background: theme.palette.common.white,
                    color: theme.palette.error.main
                  }}
                >
                  {t("delete")}
                </Button>
              </div>
            </div>
          )}

          <form onSubmit={onSubmit}>
            <fieldset disabled={requestInProgress || localRequestInProgress}>
              <TextField
                fullWidth
                disabled={requestInProgress || localRequestInProgress}
                margin="normal"
                autoComplete="off"
                id="title_input"
                label={t("title")}
                onChange={e => setName(e.target.value)}
                required
                helperText={t("food_title_helper_text")}
                value={name}
              />

              <FormControl fullWidth margin="normal">
                <InputLabel htmlFor="measured_by_label_select">
                  {t("measured_by")}
                </InputLabel>
                <Select
                  id="measured_by_label_select"
                  value={measurement}
                  disabled={requestInProgress || localRequestInProgress}
                  onChange={(e: any) => setMeasurement(e.target.value)}
                >
                  {measurings.map(measuring => (
                    <MenuItem key={measuring} value={measuring}>
                      {t("measurements." + measuring)}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <TextField
                disabled={requestInProgress || localRequestInProgress}
                fullWidth
                margin="normal"
                autoComplete="off"
                id="prots_input"
                helperText={
                  excessNutriantsError
                    ? t(
                        "amount_of_prots_fats_and_carbs_in_100_grams_of_the_product_should_not_exceed_100"
                      )
                    : t("number_or_math_expression")
                }
                error={excessNutriantsError}
                value={formulas.prots}
                onChange={e => {
                  const newValue = e.target.value;
                  setFormulas(old => ({
                    ...old,
                    prots: newValue
                  }));
                }}
                label={uiText(
                  t("prots"),
                  t(`measurements_instrumental_case_lc.${measurement}`),
                  t("grams")
                )}
              />

              <TextField
                fullWidth
                disabled={requestInProgress || localRequestInProgress}
                margin="normal"
                error={excessNutriantsError}
                autoComplete="off"
                helperText={
                  excessNutriantsError
                    ? t(
                        "amount_of_prots_fats_and_carbs_in_100_grams_of_the_product_should_not_exceed_100"
                      )
                    : t("number_or_math_expression")
                }
                id="fats_input"
                value={formulas.fats}
                onChange={e => {
                  const newValue = e.target.value;
                  setFormulas(old => ({ ...old, fats: newValue }));
                }}
                label={uiText(
                  t("fats"),
                  t(`measurements_instrumental_case_lc.${measurement}`),
                  t("grams")
                )}
              />

              <TextField
                disabled={requestInProgress || localRequestInProgress}
                fullWidth
                margin="normal"
                autoComplete="off"
                error={excessNutriantsError}
                id="carbs_input"
                value={formulas.carbs}
                helperText={
                  excessNutriantsError
                    ? t(
                        "amount_of_prots_fats_and_carbs_in_100_grams_of_the_product_should_not_exceed_100"
                      )
                    : t("number_or_math_expression")
                }
                onChange={e => {
                  const newValue = e.target.value;
                  setFormulas(old => ({ ...old, carbs: newValue }));
                }}
                label={uiText(
                  t("carbs"),
                  t(`measurements_instrumental_case_lc.${measurement}`),
                  t("grams")
                )}
              />

              <TextField
                disabled={requestInProgress || localRequestInProgress}
                fullWidth
                margin="normal"
                value={formulas.kcal}
                id="kcal"
                helperText={t("number_or_math_expression")}
                autoComplete="off"
                onChange={e => {
                  const newValue = e.target.value;
                  setFormulas(old => ({ ...old, kcal: newValue }));
                }}
                label={uiText(
                  t("kcal"),
                  t(`measurements_instrumental_case_lc.${measurement}`)
                )}
              />

              <TextField
                disabled={requestInProgress || localRequestInProgress}
                fullWidth
                margin="normal"
                id="about"
                multiline
                rows="3"
                value={about}
                label={t("about")}
                autoComplete="off"
                onChange={(e: any) => setAbout(e.target.value)}
              />

              {canBePublic && (
                <div>
                  <FormControl margin="normal">
                    <FormControlLabel
                      className={classes.checkboxLabel}
                      control={
                        <Checkbox
                          checked={isPublic}
                          disabled={requestInProgress || localRequestInProgress}
                          onChange={e => setIsPublic(e.target.checked)}
                          value="isPublic"
                          color="primary"
                        />
                      }
                      label={`${t("public")} (${t(
                        "avalible_to_copy_by_other_users"
                      )})`}
                    />
                  </FormControl>
                </div>
              )}

              <Typography gutterBottom>
                {`${t("prots")}: ${uiNumber(nutrition.prots)}${t(
                  "grams_acronym"
                )}, ${t("fats")}: ${uiNumber(nutrition.fats)}${t(
                  "grams_acronym"
                )}, ${t("carbs")}: ${uiNumber(nutrition.carbs)}${t(
                  "grams_acronym"
                )}, ${t("kcal")}: ${uiNumber(nutrition.kcal)}`}
              </Typography>

              <div>
                <Button
                  disabled={
                    requestInProgress ||
                    localRequestInProgress ||
                    excessNutriantsError
                  }
                  type="submit"
                  variant="contained"
                  color="primary"
                >
                  {isEditingMode ? t("update") : t(`create`)}
                </Button>
              </div>
            </fieldset>
          </form>
        </>
      )}
    </Page>
  );
};

export default withTheme(withRouter(connector(AddEditFood)));
