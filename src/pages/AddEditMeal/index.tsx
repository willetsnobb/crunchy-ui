import React, { useState, useEffect, FC, FormEvent } from "react";
import { withRouter, Link, RouteComponentProps } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import queryString from "query-string";
import { evaluate } from "mathjs";
import { useTranslation } from "react-i18next";
import { Autocomplete } from "@material-ui/lab";
import { KeyboardDateTimePicker } from "@material-ui/pickers";
import {
  Checkbox,
  FormControlLabel,
  FormControl,
  Icon,
  IconButton,
  withTheme,
  Typography,
  Theme,
  makeStyles,
  Button,
  WithTheme
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { getFoodsList } from "../../actions/foodsActions";
import { postMeal, editMeal, deleteMeal } from "../../actions/mealsActions";
import {
  responseIsSuccessful,
  errorIsNotFound
} from "../../utils/variousUtils";
import Page from "../../components/Page";
import { setNotification } from "../../actions/commonActions";
import paths from "../../paths/paths";
import {
  RequestStatus,
  Measurement,
  NotificationType
} from "../../utils/enums";
import { State, Food } from "../../utils/types";
import { uiNumber } from "../../utils/uiUtils";
import { backendZonedDateTime } from "../../utils/backendUtils";
import { Moment } from "moment";
import FullPageLoader from "../../components/FullPageLoader";
import mealService from "../../services/mealService";

const mapStateToProps = (state: State) => ({
  foodList: state.foods.list,
  loadingFoodList: state.foods.loadingList,
  requestInProgress:
    state.meals.postRequestStatus === RequestStatus.FETCHING ||
    state.meals.putRequestStatus === RequestStatus.FETCHING,
  deleting: state.meals.deleteRequestStatus === RequestStatus.FETCHING
});

const connectActions = {
  getFoodsList,
  postMeal,
  editMeal,
  deleteMeal,
  setNotification
};

interface PredictedMeal {
  kcal: number;
  prots: number;
  fats: number;
  carbs: number;
}

const useStyles = makeStyles((theme: Theme) => ({
  checkboxLabel: {
    color: theme.palette.text.primary
  }
}));

const connector = connect(mapStateToProps, connectActions);

type AddEditMealProps = ConnectedProps<typeof connector> &
  RouteComponentProps<{ mealId: string | undefined }> &
  WithTheme;

export type SearchParameters = {
  preselected_food_id?: string;
  preselected_amount_expression?: string;
  date?: string;
};

const AddEditMeal: FC<AddEditMealProps> = ({
  foodList,
  loadingFoodList,
  requestInProgress,
  deleting,
  getFoodsList,
  postMeal,
  editMeal,
  deleteMeal,
  setNotification,
  match: {
    params: { mealId }
  },
  location: { search },
  theme
}) => {
  const isEditingMode = !!mealId;

  const classes = useStyles();
  const { t } = useTranslation();

  const {
    // TODO use incoming date
    /*date,*/
    preselected_food_id,
    preselected_amount_expression
  }: SearchParameters = queryString.parse(search);

  const [loading, setLoading] = useState<boolean>(false);

  const [uiAmount, setUiAmount] = useState(``);
  const [initialData, setInitialData] = useState({});

  const [selectedFood, setSelectedFood] = useState<Food | null>(null);
  const [predictedMeal, setPredictedMeal] = useState<PredictedMeal | null>(
    null
  );
  const [selectedFoodId, setSelectedFoodId] = useState<string | null>(null);
  const [amount, setAmount] = useState("");
  const [amountExpr, setAmountExpr] = useState<string>(
    preselected_amount_expression || ""
  );
  const [useCurrentDateTime, setUseCurrentDateTime] = useState(true);
  const [datetime, setDatetime] = useState<any>(null);

  const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const timestamp = useCurrentDateTime
      ? backendZonedDateTime()
      : datetime && backendZonedDateTime(datetime);

    if (isEditingMode) {
      editMeal({
        ...initialData,
        food_id: Number(selectedFoodId),
        amount,
        amount_expression: amountExpr,
        timestamp
      });
    } else {
      await postMeal({
        food_id: Number(selectedFoodId),
        amount,
        amount_expression: amountExpr,
        timestamp
      });
      setSelectedFoodId(null);
      setAmountExpr("");
    }
  };

  useEffect(() => {
    if (preselected_food_id) {
      setSelectedFoodId(preselected_food_id + "");
    }
  }, [preselected_food_id]);

  useEffect(() => {
    const fetchMeal = (id: any) => {
      setLoading(true);

      mealService
        .get(id)
        .then(response => {
          if (responseIsSuccessful(response) && response.data.data) {
            setUseCurrentDateTime(false);
            setInitialData(response.data.data);
            setAmount(response.data.data.amount);
            setAmountExpr(
              response.data.data.amount_expression || response.data.data.amount
            );
            setSelectedFoodId(response.data.data.food_id);
            setDatetime(response.data.data.timestamp);
          }
        })
        .catch(e => {
          console.error(e);
          if (errorIsNotFound(e)) {
            setNotification({
              type: NotificationType.ERROR,
              message: "not_found"
            });
          } else {
            setNotification({
              type: NotificationType.ERROR,
              message: "failed"
            });
          }
        })
        .finally(() => {
          setLoading(false);
        });
    };

    if (mealId && isEditingMode) {
      fetchMeal(mealId);
    }
  }, [mealId, isEditingMode, setNotification]);

  useEffect(() => {
    getFoodsList();
  }, [getFoodsList]);

  useEffect(() => {
    if (
      selectedFoodId &&
      foodList &&
      foodList.length > 0 &&
      foodList.some((f: Food) => "" + f.id === "" + selectedFoodId)
    ) {
      const currentFood = foodList.find(
        (f: Food) => "" + f.id === "" + selectedFoodId
      );

      setSelectedFood(currentFood || null);
    } else {
      setSelectedFood(null);
    }
  }, [selectedFoodId, foodList]);

  useEffect(() => {
    if (selectedFood) {
      if (selectedFood.measurement === Measurement["1_PORTION"]) {
        setUiAmount(t(`portions`));
      } else if (selectedFood.measurement === Measurement["100_GRAM"]) {
        setUiAmount(t(`grams`));
      }
    } else {
      setUiAmount(``);
    }
  }, [selectedFood, t]);

  useEffect(() => {
    if (
      selectedFoodId &&
      foodList &&
      foodList.length > 0 &&
      foodList.some((f: Food) => "" + f.id === "" + selectedFoodId) &&
      amount
    ) {
      const currentFood =
        foodList.find((f: Food) => "" + f.id === "" + selectedFoodId) || null;

      if (currentFood) {
        if (currentFood.measurement === Measurement["1_PORTION"]) {
          setPredictedMeal({
            kcal: uiNumber(Number(currentFood.kcal) * Number(amount)),
            prots: uiNumber(Number(currentFood.prots) * Number(amount)),
            fats: uiNumber(Number(currentFood.fats) * Number(amount)),
            carbs: uiNumber(Number(currentFood.carbs) * Number(amount))
          });
        } else if (currentFood.measurement === Measurement["100_GRAM"]) {
          setPredictedMeal({
            kcal: uiNumber((Number(amount) * Number(currentFood.kcal)) / 100),
            prots: uiNumber((Number(amount) * Number(currentFood.prots)) / 100),
            fats: uiNumber((Number(amount) * Number(currentFood.fats)) / 100),
            carbs: uiNumber((Number(amount) * Number(currentFood.carbs)) / 100)
          });
        }
      }
    } else {
      setPredictedMeal(null);
    }
  }, [selectedFoodId, foodList, amount]);

  useEffect(() => {
    if (amountExpr) {
      try {
        const calcedAmount = evaluate(amountExpr.replace(",", "."));
        setAmount(calcedAmount);
      } catch {}
    } else {
      setAmount("");
    }
  }, [amountExpr]);

  const loadingSomething = loading || loadingFoodList;

  return (
    <Page
      style={{ padding: theme.spacing(3) }}
      requestInProgress={requestInProgress}
      topAppBarIcons={
        isEditingMode &&
        !requestInProgress && (
          <IconButton
            disabled={deleting}
            aria-label={t("delete")}
            onClick={() => {
              deleteMeal(Number(mealId));
            }}
            color="inherit"
            style={{ cursor: "pointer" }}
          >
            <Icon>delete</Icon>
          </IconButton>
        )
      }
    >
      {loadingSomething && <FullPageLoader />}

      {!loadingSomething && (
        <>
          {foodList && foodList.length === 0 && (
            <>
              <Typography gutterBottom>
                {t("you_dont_have_any_foods_so_you_cant_create_meal")}
              </Typography>

              <Typography>
                <Link
                  style={{ color: theme.palette.text.primary }}
                  to={paths.Foods.toPath()}
                >
                  {t("search_or_add_some_foods")}
                </Link>
              </Typography>
            </>
          )}

          {foodList && foodList.length > 0 && (
            <form autoComplete="off" onSubmit={onSubmit}>
              <fieldset disabled={requestInProgress}>
                <Autocomplete
                  id="food_selector"
                  options={foodList}
                  renderInput={params => (
                    <TextField
                      {...params}
                      margin="dense"
                      label={t("food")}
                      fullWidth
                    />
                  )}
                  onChange={(e: any, v: Food | null) => {
                    if (v === null) {
                      setSelectedFoodId(null);
                    } else {
                      setSelectedFoodId("" + v.id);
                    }
                  }}
                  getOptionLabel={(o: Food) => {
                    return o.name;
                  }}
                  getOptionSelected={(option: Food, value: Food) => {
                    return "" + value.id === option.id + "";
                  }}
                  value={selectedFood}
                />

                <TextField
                  margin="dense"
                  fullWidth
                  id="amount"
                  autoComplete="off"
                  label={
                    uiAmount ? `${t("amount")} (${uiAmount})` : t("amount")
                  }
                  value={amountExpr}
                  onChange={(e: any) => setAmountExpr(e.target.value)}
                  helperText={t("number_or_math_expression")}
                />

                <FormControl margin="dense">
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={useCurrentDateTime}
                        onChange={e => setUseCurrentDateTime(e.target.checked)}
                        value="use_current_datetime"
                        color="primary"
                      />
                    }
                    className={classes.checkboxLabel}
                    label={t("use_current_datetime")}
                  />
                </FormControl>

                {!useCurrentDateTime && (
                  <div>
                    <KeyboardDateTimePicker
                      id="datetime-of-meal"
                      required
                      margin="dense"
                      ampm={false}
                      invalidDateMessage={t("invalid_date_format")}
                      value={datetime}
                      onChange={(date: Moment) => setDatetime(date.format())}
                      label={t("date_and_time")}
                      onError={e => console.log(e)}
                      format="YYYY/MM/DD HH:mm"
                    />
                  </div>
                )}

                {selectedFood && (
                  <Table style={{ width: "100%", marginTop: theme.spacing(2) }}>
                    <TableBody>
                      <TableRow>
                        <TableCell align="center" colSpan={3}>{`${t(
                          "selected_food"
                        )}: ${selectedFood.name}`}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{` `}</TableCell>
                        <TableCell>
                          {t(`measurements.${selectedFood.measurement}`)}
                        </TableCell>
                        <TableCell>
                          {amount && uiAmount
                            ? `${amount} ${uiAmount} ${t("meal")}`
                            : t("meal")}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{t("kcal")}</TableCell>
                        <TableCell>{uiNumber(selectedFood.kcal)}</TableCell>
                        <TableCell>
                          {predictedMeal ? predictedMeal.kcal : "--"}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{`${t("prots")} (${t(
                          "grams_acronym"
                        )})`}</TableCell>
                        <TableCell>{uiNumber(selectedFood.prots)}</TableCell>
                        <TableCell>
                          {predictedMeal ? predictedMeal.prots : "--"}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{`${t("fats")} (${t(
                          "grams_acronym"
                        )})`}</TableCell>
                        <TableCell>{uiNumber(selectedFood.fats)}</TableCell>
                        <TableCell>
                          {predictedMeal ? predictedMeal.fats : "--"}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{`${t("carbs")} (${t(
                          "grams_acronym"
                        )})`}</TableCell>
                        <TableCell>{selectedFood.carbs}</TableCell>
                        <TableCell>
                          {predictedMeal ? predictedMeal.carbs : "--"}
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                )}
                <div style={{ paddingTop: theme.spacing(2) }}>
                  <Button
                    color="primary"
                    type="submit"
                    variant="contained"
                    disabled={!selectedFoodId || !amount || requestInProgress}
                  >
                    {isEditingMode && t(`update`)}
                    {!isEditingMode && t(`add_meal`)}
                  </Button>
                </div>
              </fieldset>
            </form>
          )}
        </>
      )}
    </Page>
  );
};

export default withTheme(withRouter(connector(AddEditMeal)));
