import React, { FC } from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import paths from "../../../paths/paths";
import { uiNumber } from "../../../utils/uiUtils";
import {
  Card,
  CardContent,
  CardActions,
  Button,
  Typography
} from "@material-ui/core";
import { DailyMealsSummary } from "../../../utils/types";

type MealListItem = {
  item: DailyMealsSummary;
}

const MealListItem: FC<MealListItem> = ({ item }) => {
  const { t } = useTranslation();

  const totalIsNotZero = !!(
    Number(item.protsPercent) +
    Number(item.fatsPercent) +
    Number(item.carbsPercent)
  );

  return (
    <Card>
      <CardContent style={{ paddingBottom: 0 }}>
        <Typography variant="h6">{item.uiDate}</Typography>
        <Typography>
          {t("kcal")}: {uiNumber(item.kcal)}
        </Typography>
        <Typography>
          {t("prots")}: {uiNumber(item.prots)}
          {t("grams_acronym")}
          {totalIsNotZero && ` (${item.protsPercent}%)`}
        </Typography>
        <Typography>
          {t("fats")}: {uiNumber(item.fats)}
          {t("grams_acronym")}
          {totalIsNotZero && ` (${item.fatsPercent}%)`}
        </Typography>
        <Typography>
          {t("carbs")}: {uiNumber(item.carbs)}
          {t("grams_acronym")}
          {totalIsNotZero && ` (${item.carbsPercent}%)`}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          component={Link}
          to={paths.MealsByDate.toPath({ date: item.date })}
        >
          {t("see_meals")}
        </Button>
      </CardActions>
    </Card>
  );
};

export default MealListItem;
