import React, { FC, useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import moment from "moment";
import { useTranslation } from "react-i18next";
import { Line } from "react-chartjs-2";
import {
  Fab,
  Typography,
  withTheme,
  Card,
  WithTheme,
  Divider,
  FormControl,
  Select,
  MenuItem,
  useMediaQuery,
  Paper,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableRow,
  Link as MuiLink,
  Tooltip,
  makeStyles
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { amber, blue, green, purple } from "@material-ui/core/colors";
import MealListItem from "./MealListItem";
import paths from "../../paths/paths";
import {
  getMealsList,
  setMealInterval,
  GetMealsType
} from "../../actions/mealsActions";
import Page from "../../components/Page";
import LoadingSpinner from "../../components/LoadingSpinner";
import { RequestStatus, MealIntervalOption } from "../../utils/enums";

import { State, Meal, DailyMealsSummary } from "../../utils/types";
import { uiDate, uiNumber } from "../../utils/uiUtils";
import InfoCard from "../../components/InfoCard";
import GridItem from "../../components/GridItem";
import GridContainer from "../../components/GridContainer";

const useStyles = makeStyles({
  addMealLink: {
    textTransform: "lowercase",
    "&::first-letter": {
      textTransform: "uppercase"
    }
  }
});

const connector = connect(
  (state: State) => ({
    mealsList: state.meals.list,
    loading: state.meals.getRequestStatus === RequestStatus.FETCHING,
    mealInterval: state.meals.interval,
    useTables: state.common.useTables
  }),
  { getMealsList, setMealInterval }
);

type MealsProps = ConnectedProps<typeof connector> & WithTheme;

interface Totals {
  prots: number;
  fats: number;
  carbs: number;
}

const Meals: FC<MealsProps> = ({
  mealsList,
  loading,
  getMealsList,

  mealInterval,
  setMealInterval,

  useTables,

  theme
}) => {
  const classes = useStyles();
  const { t } = useTranslation();

  const smallest = useMediaQuery(theme.breakpoints.down("xs"));

  const [averageKcal, setAverageKcal] = useState(0);
  const [totals, setTotals] = useState<Totals>({ prots: 0, fats: 0, carbs: 0 });
  const [dailyInfo, setDailyInfo] = useState<DailyMealsSummary[]>([]);

  useEffect(() => {
    getMealsList(GetMealsType.BY_INTERVAL, { intervalOption: mealInterval });
  }, [getMealsList, mealInterval]);

  useEffect(() => {
    if (mealsList && mealsList.length > 0) {
      const infoByDate: any = {};

      mealsList.forEach((meal: Meal) => {
        const mealDate: string = moment(meal.timestamp).format("YYYY-MM-DD");

        if (infoByDate[mealDate]) {
          infoByDate[mealDate].kcal += Number(meal.kcal);
          infoByDate[mealDate].prots += Number(meal.prots);
          infoByDate[mealDate].fats += Number(meal.fats);
          infoByDate[mealDate].carbs += Number(meal.carbs);
        } else {
          infoByDate[mealDate] = {
            kcal: Number(meal.kcal),
            prots: Number(meal.prots),
            fats: Number(meal.fats),
            carbs: Number(meal.carbs),
            uiDate: uiDate(meal.timestamp),
            date: mealDate
          };
        }
      });

      const absoluteTotals: Totals = { prots: 0, fats: 0, carbs: 0 };
      let totalKcal = 0;

      const newDailyInfo = Object.values(infoByDate).map((item: any) => {
        const totalNutrients = item.prots + item.fats + item.carbs;

        absoluteTotals.prots += item.prots;
        absoluteTotals.fats += item.fats;
        absoluteTotals.carbs += item.carbs;

        totalKcal += item.kcal;

        let protsPercent = 0;
        let fatsPercent = 0;
        let carbsPercent = 0;

        if (totalNutrients > 0) {
          protsPercent = uiNumber((item.prots / totalNutrients) * 100);
          fatsPercent = uiNumber((item.fats / totalNutrients) * 100);
          carbsPercent = uiNumber(100 - protsPercent - fatsPercent);
        }

        return { ...item, protsPercent, fatsPercent, carbsPercent };
      });

      setAverageKcal(uiNumber(totalKcal / newDailyInfo.length));
      setDailyInfo(newDailyInfo);

      const absoluteTotalNutrients =
        absoluteTotals.prots + absoluteTotals.fats + absoluteTotals.carbs;

      let totalProtsPercent = 0;
      let totalFatsPercent = 0;
      let totalCarbsPercent = 0;

      if (absoluteTotalNutrients > 0) {
        totalProtsPercent = uiNumber(
          (absoluteTotals.prots / absoluteTotalNutrients) * 100
        );
        totalFatsPercent = uiNumber(
          (absoluteTotals.fats / absoluteTotalNutrients) * 100
        );
        totalCarbsPercent = uiNumber(
          100 - totalProtsPercent - totalFatsPercent
        );
      }

      const percentTotals: Totals = {
        prots: totalProtsPercent,
        fats: totalFatsPercent,
        carbs: totalCarbsPercent
      };

      setTotals(percentTotals);
    } else {
      setDailyInfo([]);
    }
  }, [mealsList, t]);

  const gotFetchedMeals =
    dailyInfo && dailyInfo.length > 0 && false === loading;

  const labels = dailyInfo
    .map((dayMealInfo: any) => dayMealInfo.uiDate)
    .reverse();

  return (
    <Page
      style={{ padding: theme.spacing(3), color: theme.palette.text.primary }}
    >
      <div style={{ paddingBottom: theme.spacing(3) }}>
        <FormControl>
          <Select
            value={mealInterval}
            onChange={(e: any) => {
              const option: MealIntervalOption = e.target.value;
              setMealInterval(option);
            }}
          >
            <MenuItem value={MealIntervalOption.THIS_WEEK}>
              {t("meal_interval_option." + MealIntervalOption.THIS_WEEK)}
            </MenuItem>
            <MenuItem value={MealIntervalOption.LAST_WEEK}>
              {t("meal_interval_option." + MealIntervalOption.LAST_WEEK)}
            </MenuItem>
            <MenuItem value={MealIntervalOption.THIS_MONTH}>
              {t("meal_interval_option." + MealIntervalOption.THIS_MONTH)}
            </MenuItem>
            <MenuItem value={MealIntervalOption.LAST_MONTH}>
              {t("meal_interval_option." + MealIntervalOption.LAST_MONTH)}
            </MenuItem>
            <MenuItem value={MealIntervalOption.LIFETIME}>
              {t("meal_interval_option." + MealIntervalOption.LIFETIME)}
            </MenuItem>
          </Select>
        </FormControl>
      </div>

      {(!dailyInfo || dailyInfo.length === 0) && false === loading && (
        <>
          <Typography gutterBottom>{t("no_meals_found")}</Typography>
          <Typography className={classes.addMealLink}>
            <Link
              style={{ color: theme.palette.text.primary }}
              to={paths.AddMeal.toPath()}
            >
              {t("add")} {t("meal")}
            </Link>
          </Typography>
        </>
      )}

      {gotFetchedMeals && (
        <>
          {smallest && (
            <Card
              style={{
                background: theme.palette.primary.main,
                color: theme.palette.common.white,
                padding: theme.spacing(3)
              }}
            >
              <Typography gutterBottom>
                {`${t("kcal")} (${t("average")}): ${averageKcal}`}
              </Typography>
              <Typography gutterBottom>
                {`${t("prots")} ${totals.prots}%`}
              </Typography>
              <Typography gutterBottom>
                {`${t("fats")} ${totals.fats}%`}
              </Typography>
              <Typography gutterBottom>
                {`${t("carbs")} ${totals.carbs}%`}
              </Typography>
            </Card>
          )}

          {!smallest && (
            <GridContainer>
              <GridItem>
                <InfoCard
                  title={`${t("kcal")} (${t("average")})`}
                  value={averageKcal}
                />
              </GridItem>
              <GridItem>
                <InfoCard title={t("prots")} value={`${totals.prots}%`} />
              </GridItem>
              <GridItem>
                <InfoCard title={t("fats")} value={`${totals.fats}%`} />
              </GridItem>
              <GridItem>
                <InfoCard title={t("carbs")} value={`${totals.carbs}%`} />
              </GridItem>
            </GridContainer>
          )}

          <Divider
            style={{
              margin: 0,
              marginTop: theme.spacing(3),
              height: 0.01, // zero height messes up margins. non-zero fixes it
              opacity: 0
            }}
          />

          <GridContainer>
            <GridItem size="middle">
              <Card
                style={{
                  padding: theme.spacing(2),
                  position: "relative",
                  boxSizing: "border-box",
                  height: 300
                }}
              >
                <Line
                  data={{
                    labels,
                    datasets: [
                      {
                        label: t("kcal"),
                        borderColor: purple[600],
                        data: dailyInfo
                          .map((dayMealInfo: any) => uiNumber(dayMealInfo.kcal))
                          .reverse()
                      }
                    ]
                  }}
                  options={{
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: { fontColor: theme.palette.text.secondary }
                    },
                    scales: {
                      xAxes: [
                        { ticks: { fontColor: theme.palette.text.secondary } }
                      ],
                      yAxes: [
                        {
                          ticks: {
                            fontColor: theme.palette.text.secondary,
                            beginAtZero: true
                          }
                        }
                      ]
                    }
                  }}
                />
              </Card>
            </GridItem>

            <GridItem size="middle">
              <Card
                style={{
                  padding: theme.spacing(2),
                  position: "relative",
                  boxSizing: "border-box",
                  height: 300
                }}
              >
                <Line
                  data={{
                    labels,
                    datasets: [
                      {
                        label: `${t("prots")}, %`,
                        borderColor: amber[600],
                        data: dailyInfo
                          .map((dayMealInfo: any) =>
                            uiNumber(dayMealInfo.protsPercent)
                          )
                          .reverse()
                      },
                      {
                        label: `${t("fats")}, %`,
                        borderColor: green[600],
                        data: dailyInfo
                          .map((dayMealInfo: any) =>
                            uiNumber(dayMealInfo.fatsPercent)
                          )
                          .reverse()
                      },
                      {
                        label: `${t("carbs")}, %`,
                        borderColor: blue[600],
                        data: dailyInfo
                          .map((dayMealInfo: any) =>
                            uiNumber(dayMealInfo.carbsPercent)
                          )
                          .reverse()
                      }
                    ]
                  }}
                  options={{
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: { fontColor: theme.palette.text.secondary }
                    },
                    scales: {
                      xAxes: [
                        { ticks: { fontColor: theme.palette.text.secondary } }
                      ],
                      yAxes: [
                        {
                          ticks: {
                            fontColor: theme.palette.text.secondary,
                            beginAtZero: true,
                            callback: function(value, index, values) {
                              return value + "%";
                            }
                          }
                        }
                      ]
                    }
                  }}
                />
              </Card>
            </GridItem>
          </GridContainer>

          <Divider
            style={{
              marginTop: theme.spacing(3),
              marginBottom: theme.spacing(3)
            }}
          />

          {!useTables && (
            <GridContainer>
              {dailyInfo.map(item => (
                <GridItem key={item.uiDate}>
                  <MealListItem item={item} />
                </GridItem>
              ))}
            </GridContainer>
          )}

          {useTables && (
            <Paper style={{ overflow: "hidden" }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>{t("date")}</TableCell>
                    <TableCell>{t("kcal")}</TableCell>
                    <TableCell>{t("prots")}</TableCell>
                    <TableCell>{t("fats")}</TableCell>
                    <TableCell>{t("carbs")}</TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {dailyInfo.map(item => {
                    const totalIsNotZero = !!(
                      Number(item.protsPercent) +
                      Number(item.fatsPercent) +
                      Number(item.carbsPercent)
                    );

                    return (
                      <TableRow key={item.uiDate}>
                        <TableCell>
                          <Tooltip title={t("see_meals")}>
                            <MuiLink
                              to={paths.MealsByDate.toPath({
                                date: item.date
                              })}
                              style={{
                                textDecoration: "underline",
                                color: theme.palette.text.primary
                              }}
                              component={Link}
                            >
                              {item.uiDate}
                            </MuiLink>
                          </Tooltip>
                        </TableCell>
                        <TableCell>{uiNumber(item.kcal)}</TableCell>
                        <TableCell>
                          {uiNumber(item.prots)}
                          {t("grams_acronym")}
                          {totalIsNotZero && ` (${item.protsPercent}%)`}
                        </TableCell>
                        <TableCell>
                          {uiNumber(item.fats)}
                          {t("grams_acronym")}
                          {totalIsNotZero && ` (${item.fatsPercent}%)`}
                        </TableCell>
                        <TableCell>
                          {uiNumber(item.carbs)}
                          {t("grams_acronym")}
                          {totalIsNotZero && ` (${item.carbsPercent}%)`}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </Paper>
          )}
        </>
      )}

      {true === loading && <LoadingSpinner />}

      <Fab
        style={{
          position: "fixed",
          bottom: theme.spacing(3),
          right: theme.spacing(3)
        }}
        component={Link}
        to={paths.AddMeal.toPath()}
        color="secondary"
        aria-label={t("add_meal")}
      >
        <AddIcon />
      </Fab>
    </Page>
  );
};

export default withTheme(connector(Meals));
