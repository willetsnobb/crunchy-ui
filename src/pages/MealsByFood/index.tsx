import React, { FC, useEffect, useState } from "react";
import { ConnectedProps, connect } from "react-redux";
import qs from "query-string";
import { withRouter, RouteComponentProps, Link } from "react-router-dom";
import {
  withTheme,
  WithTheme,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Paper,
  TableHead,
  IconButton,
  Icon,
  Tooltip
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
import Page from "../../components/Page";
import { Meal, Food, State } from "../../utils/types";
import { setNotification } from "../../actions/commonActions";
import { NotificationType, Measurement } from "../../utils/enums";
import LoadingSpinner from "../../components/LoadingSpinner";
import MealListItem from "../../components/MealListItem";
import GridContainer from "../../components/GridContainer";
import GridItem from "../../components/GridItem";
import { uiDateTime, uiNumber } from "../../utils/uiUtils";
import paths from "../../paths/paths";
import { SearchParameters } from "../AddEditMeal";
import mealService from "../../services/mealService";
import foodService from "../../services/foodService";

const connector = connect((state: State) => ({
  useTables: state.common.useTables
}));

type MealsByFood = WithTheme &
  RouteComponentProps<{
    foodId: string;
  }> &
  ConnectedProps<typeof connector>;

const MealsByFood: FC<MealsByFood> = ({
  useTables,
  match: {
    params: { foodId }
  },
  theme
}) => {
  const { t } = useTranslation();

  const [meals, setMeals] = useState<Meal[]>([]);
  const [food, setFood] = useState<Food | null>(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getAll = () => {
      setLoading(true);

      const getFood = foodService.get(foodId).then(response => {
        const currentFood: Food =
          response && response.data && response.data.data;

        return currentFood;
      });

      const getMeals = mealService.getByFood(foodId).then(response => {
        const foodMeals: Meal[] =
          response && response.data && response.data.data;
        setMeals(foodMeals);

        return foodMeals;
      });

      Promise.all([getMeals, getFood])
        .then(function([rawMeals = [], food]: [Meal[] | void, Food]) {
          setFood(food);

          setMeals(
            rawMeals.map(
              (meal: Meal): Meal => {
                const calcProps = () => {
                  type MissingFoodProps = {
                    kcal?: Food["kcal"];
                    prots?: Food["prots"];
                    fats?: Food["fats"];
                    carbs?: Food["carbs"];
                  };

                  const props: MissingFoodProps = {};

                  if (food.measurement === Measurement["100_GRAM"]) {
                    props.kcal =
                      (Number(meal.amount) * Number(food.kcal)) / 100;
                    props.prots =
                      (Number(meal.amount) * Number(food.prots)) / 100;
                    props.fats =
                      (Number(meal.amount) * Number(food.fats)) / 100;
                    props.carbs =
                      (Number(meal.amount) * Number(food.carbs)) / 100;
                  } else if (food.measurement === Measurement["1_PORTION"]) {
                    props.kcal = Number(meal.amount) * Number(food.kcal);
                    props.prots = Number(meal.amount) * Number(food.prots);
                    props.fats = Number(meal.amount) * Number(food.fats);
                    props.carbs = Number(meal.amount) * Number(food.carbs);
                  }

                  return props;
                };

                return {
                  ...meal,
                  name: food.name,
                  measurement: food.measurement,
                  ...calcProps()
                };
              }
            )
          );
        })
        .catch(e => {
          setNotification({
            type: NotificationType.ERROR,
            message: "failed"
          });
        })
        .finally(() => {
          setLoading(false);
        });
    };

    getAll();
  }, [foodId]);

  const uiAmountUnitFromMeal = (meal: Meal) => {
    if (meal.measurement === Measurement["100_GRAM"]) {
      return t("grams");
    } else if (meal.measurement === Measurement["1_PORTION"]) {
      return t("portions");
    }
  };

  return (
    <Page style={{ padding: theme.spacing(3) }}>
      {loading && <LoadingSpinner />}

      {!loading && meals.length === 0 && <>{t("no_items_found")}</>}

      {!loading && meals.length > 0 && (
        <>
          <Typography
            style={{ paddingBottom: theme.spacing(3) }}
            variant="h6"
            component="h1"
          >
            {t("my_meals")}: {food && food.name}
          </Typography>

          {useTables && (
            <Paper style={{ overflow: "hidden" }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>{t("actions")}</TableCell>
                    <TableCell>{t("date_and_time")}</TableCell>
                    <TableCell>{t("amount")}</TableCell>
                    <TableCell>{t("kcal")}</TableCell>
                    <TableCell>
                      {`${t("prots")}, ${t("grams_acronym")}`}
                    </TableCell>
                    <TableCell>
                      {`${t("fats")}, ${t("grams_acronym")}`}
                    </TableCell>
                    <TableCell>
                      {`${t("carbs")}, ${t("grams_acronym")}`}
                    </TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {meals.map((meal: Meal) => {
                    const repeatMealSearchParams: SearchParameters = {
                      preselected_food_id: `${meal.food_id}`,
                      preselected_amount_expression: `${meal.amount_expression ||
                        meal.amount}`
                    };

                    return (
                      <TableRow key={meal.id}>
                        <TableCell
                          style={{
                            paddingTop: 0,
                            paddingBottom: 0,
                            width: 130
                          }}
                        >
                          <Tooltip title={t("edit")}>
                            <Link
                              to={paths.EditMeal.toPath({
                                mealId: meal.id
                              })}
                              style={{
                                textDecoration: "none"
                              }}
                            >
                              <IconButton>
                                <Icon>edit</Icon>
                              </IconButton>
                            </Link>
                          </Tooltip>
                          <Tooltip title={t("repeat")}>
                            <Link
                              to={`${paths.AddMeal.toPath({
                                mealId: meal.id
                              })}?${qs.stringify(repeatMealSearchParams)}`}
                              style={{
                                textDecoration: "none"
                              }}
                            >
                              <IconButton>
                                <Icon>autorenew</Icon>
                              </IconButton>
                            </Link>
                          </Tooltip>
                        </TableCell>
                        <TableCell>{uiDateTime(meal.timestamp)}</TableCell>
                        <TableCell>{`${uiNumber(
                          meal.amount
                        )} ${uiAmountUnitFromMeal(meal)}`}</TableCell>
                        <TableCell>{uiNumber(meal.kcal)}</TableCell>
                        <TableCell>{uiNumber(meal.prots)}</TableCell>
                        <TableCell>{uiNumber(meal.fats)}</TableCell>
                        <TableCell>{uiNumber(meal.carbs)}</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </Paper>
          )}

          {!useTables && (
            <GridContainer>
              {meals.map((meal: Meal) => (
                <GridItem key={meal.id}>
                  <MealListItem showDate item={meal} />
                </GridItem>
              ))}
            </GridContainer>
          )}
        </>
      )}
    </Page>
  );
};

export default withTheme(withRouter(connector(MealsByFood)));
