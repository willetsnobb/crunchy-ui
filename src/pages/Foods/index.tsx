import React, { useEffect, FC, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import {
  IconButton,
  Icon,
  withTheme,
  TextField,
  WithTheme
} from "@material-ui/core";
import { getFoodsList } from "../../actions/foodsActions";
import Page from "../../components/Page";
import paths from "../../paths/paths";
import { State } from "../../utils/types";
import MyFoods from "./MyFoods";
import PublicFoods from "./PublicFoods";

const connector = connect(
  (state: State) => ({
    foodsList: state.foods.list,
    loading: state.foods.loadingList
  }),
  { getFoodsList }
);

type FoodsProps = WithTheme & ConnectedProps<typeof connector>;

const Foods: FC<FoodsProps> = ({ foodsList, loading, getFoodsList, theme }) => {
  const { t } = useTranslation();

  const [searchText, setSearchText] = useState<string>("");

  useEffect(() => {
    getFoodsList();
  }, [getFoodsList]);

  return (
    <Page
      style={{ padding: theme.spacing(3), color: theme.palette.text.primary }}
    >
      <div style={{ display: "flex" }}>
        <div style={{ flex: 1 }}>
          <TextField
            autoComplete="off"
            value={searchText}
            id="search_foods"
            label={t("search_foods")}
            style={{ margin: "0 auto", display: "flex" }}
            onChange={e => setSearchText(e.target.value)}
          />
        </div>
        {searchText && (
          <div style={{ paddingLeft: theme.spacing(2), display: "flex" }}>
            <div style={{ margin: "auto" }}>
              <IconButton
                onClick={() => {
                  setSearchText("");
                }}
                aria-label={t("clear")}
              >
                <Icon color="action">clear</Icon>
              </IconButton>
            </div>
          </div>
        )}
      </div>

      <MyFoods isLoading={loading} items={foodsList} filter={searchText} />

      <PublicFoods filter={searchText} hideLoader={loading} />

      <Fab
        style={{
          position: "fixed",
          bottom: theme.spacing(3),
          right: theme.spacing(3)
        }}
        component={Link}
        to={paths.AddFood.toPath()}
        color="secondary"
        aria-label={t("add")}
      >
        <AddIcon />
      </Fab>
    </Page>
  );
};

export default withTheme(connector(Foods));
