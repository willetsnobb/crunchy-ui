import React, { FC, useState, useEffect, useCallback } from "react";
import {
  makeStyles,
  Theme,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { connect, ConnectedProps } from "react-redux";

import { setNotification } from "../../../actions/commonActions";

import { Food, State } from "../../../utils/types";
import foodService from "../../../services/foodService";
import { responseIsSuccessful } from "../../../utils/variousUtils";
import { NotificationType, Impression } from "../../../utils/enums";
import LoadingSpinner from "../../../components/LoadingSpinner";
import GridContainer from "../../../components/GridContainer";
import GridItem from "../../../components/GridItem";
import FoodCard from "./FoodCard";
import FoodTableRow from "./FoodTableRow";
import { copyFood } from "../../../actions/foodsActions";

const SEARCH_PUBLIC_FOODS_TIMEOUT_MS = 350;

const useStyles = makeStyles((theme: Theme) => ({
  itemsGroupLabel: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(2)
  }
}));

const connector = connect(
  (state: State) => ({
    useTables: state.common.useTables,
    copyingFoods: state.foods.copying
  }),
  { setNotification, copyFood }
);

type OwnProps = { filter: string; hideLoader: boolean };

type PublicFoodsProps = OwnProps & ConnectedProps<typeof connector>;

const PublicFoods: FC<PublicFoodsProps> = ({
  useTables,
  setNotification,
  copyingFoods,
  copyFood,
  filter,
  hideLoader
}) => {
  const classes = useStyles();
  const { t } = useTranslation();

  const [publicFoods, setPublicFoods] = useState<Food[]>([]);
  const [publicSearchIsInProgress, setPublicSearchIsInProgress] = useState<
    boolean
  >(false);
  const [publicFoodSearchTimeout, setPublicFoodSearchTimeout] = useState<any>(
    null
  );
  const [
    impressionsForFoodsInProgress,
    setImpressionsForFoodsInProgress
  ] = useState<Food["id"][]>([]);
  const [lastSearchFilter, setLastSearchFilter] = useState<string>(filter);

  const addImpression = async (impression: Impression, foodId: Food["id"]) => {
    if (impressionsForFoodsInProgress.some(id => id === foodId)) {
      return setNotification({
        type: NotificationType.ERROR,
        message: "this_request_is_already_in_progress"
      });
    }

    setImpressionsForFoodsInProgress(old => [...old, foodId]);

    foodService
      .addImpression(foodId, impression)
      .then(response => {
        if (responseIsSuccessful(response)) {
          setPublicFoods(old =>
            old.map(i => {
              if (Number(i.id) !== Number(foodId)) {
                return i;
              }

              if (impression === Impression.LIKE) {
                if (i.my_impression === Impression.DISLIKE) {
                  return {
                    ...i,
                    my_impression: impression,
                    likes: Number(i.likes) + 1,
                    dislikes: Number(i.dislikes) - 1
                  };
                } else {
                  return {
                    ...i,
                    my_impression: impression,
                    likes: Number(i.likes) + 1
                  };
                }
              } else if (impression === Impression.DISLIKE) {
                if (i.my_impression === Impression.LIKE) {
                  return {
                    ...i,
                    my_impression: impression,
                    dislikes: Number(i.dislikes) + 1,
                    likes: Number(i.likes) - 1
                  };
                } else {
                  return {
                    ...i,
                    my_impression: impression,
                    dislikes: Number(i.dislikes) + 1
                  };
                }
              } else {
                return { ...i, my_impression: impression };
              }
            })
          );
        }
      })
      .catch(e => {
        console.error(e);
      })
      .finally(() => {
        setImpressionsForFoodsInProgress(old =>
          old.filter(id => id !== foodId)
        );
      });
  };

  const removeImpression = async (foodId: Food["id"]) => {
    if (impressionsForFoodsInProgress.some(id => id === foodId)) {
      return setNotification({
        type: NotificationType.ERROR,
        message: "this_request_is_already_in_progress"
      });
    }

    setImpressionsForFoodsInProgress(old => [...old, foodId]);

    foodService
      .removeImpression(foodId)
      .then(response => {
        if (responseIsSuccessful(response)) {
          setPublicFoods(old =>
            old.map(i => {
              if (Number(i.id) !== Number(foodId)) {
                return i;
              }

              if (i.my_impression === Impression.LIKE) {
                return {
                  ...i,
                  my_impression: null,
                  likes: Number(i.likes) - 1
                };
              } else if (i.my_impression === Impression.DISLIKE) {
                return {
                  ...i,
                  my_impression: null,
                  dislikes: Number(i.dislikes) - 1
                };
              } else {
                return { ...i, my_impression: null };
              }
            })
          );
        }
      })
      .catch(e => {
        console.error(e);
      })
      .finally(() => {
        setImpressionsForFoodsInProgress(old =>
          old.filter(id => id !== foodId)
        );
      });
  };

  const searchPublicFoods = useCallback(
    (searchText: string) => {
      setPublicFoods([]);

      if (!searchText) {
        return;
      }

      setPublicSearchIsInProgress(true);

      foodService
        .searchPublicFoods(searchText)
        .then(response => {
          if (responseIsSuccessful(response)) {
            const foods: Food[] = response.data.data;

            setPublicFoods(
              foods.map((f: Food): Food => ({ ...f, loadingImpressions: true }))
            );

            foods.forEach(f => {
              foodService
                .getImpressions(f.id)
                .then(response => {
                  if (responseIsSuccessful(response)) {
                    const {
                      likes,
                      dislikes,
                      my_impression
                    } = response.data.data;

                    setPublicFoods((foods: Food[]) =>
                      foods.map(
                        (food: Food): Food =>
                          food.id === f.id
                            ? {
                                ...food,
                                likes,
                                dislikes,
                                my_impression,
                                loadingImpressions: false
                              }
                            : food
                      )
                    );
                  }
                })
                .catch(e => {
                  console.error(e);
                  setNotification({
                    message: "failed",
                    type: NotificationType.ERROR
                  });
                });
            });
          }
        })
        .catch(e => {
          console.error(e);
          setNotification({
            message: "failed",
            type: NotificationType.ERROR
          });
        })
        .finally(() => {
          setPublicSearchIsInProgress(false);
        });
    },
    [setNotification]
  );

  useEffect(() => {
    if (filter) {
      if (filter !== lastSearchFilter) {
        clearTimeout(publicFoodSearchTimeout);
        setPublicFoodSearchTimeout(null);

        setPublicFoodSearchTimeout(
          setTimeout(() => {
            searchPublicFoods(filter);
            setPublicFoodSearchTimeout(null);
          }, SEARCH_PUBLIC_FOODS_TIMEOUT_MS)
        );
      }
    } else {
      setPublicFoods([]);

      clearTimeout(publicFoodSearchTimeout);
      setPublicFoodSearchTimeout(null);
    }

    if (filter !== lastSearchFilter) {
      setLastSearchFilter(filter);
    }
  }, [filter, lastSearchFilter, publicFoodSearchTimeout, searchPublicFoods]);

  const loadingPublicFoods = !!(
    publicSearchIsInProgress || !!publicFoodSearchTimeout
  );

  return loadingPublicFoods ? (
    <>
      {!hideLoader && (
        <>
          <Typography className={classes.itemsGroupLabel}>
            {t("public_foods")}
          </Typography>
          <LoadingSpinner />
        </>
      )}
    </>
  ) : (
    <>
      {publicFoods.length > 0 ? (
        <>
          <Typography className={classes.itemsGroupLabel}>
            {t("public_foods")}
          </Typography>
          {useTables && (
            <Paper>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>{t("actions")}</TableCell>

                    <TableCell>{t("likes")}</TableCell>
                    <TableCell>{t("dislikes")}</TableCell>

                    <TableCell>{t("name")}</TableCell>
                    <TableCell>{t("description")}</TableCell>

                    <TableCell>{t("measurement")}</TableCell>

                    <TableCell>{t("kcal")}</TableCell>

                    <TableCell>
                      {t("prots")}, {t("grams_acronym")}
                    </TableCell>
                    <TableCell>
                      {t("fats")}, {t("grams_acronym")}
                    </TableCell>
                    <TableCell>
                      {t("carbs")}, {t("grams_acronym")}
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {publicFoods.map((item: Food) => (
                    <FoodTableRow
                      key={item.id}
                      item={item}
                      copyFoodById={copyFood}
                      addImpression={addImpression}
                      removeImpression={removeImpression}
                      impressionRequestInProgress={impressionsForFoodsInProgress.some(
                        id => `${id}` === `${item.id}`
                      )}
                      copying={copyingFoods.some(
                        fId => `${fId}` === `${item.id}`
                      )}
                    />
                  ))}
                </TableBody>
              </Table>
            </Paper>
          )}
          {!useTables && (
            <GridContainer>
              {publicFoods.map((item: Food) => (
                <GridItem key={item.id}>
                  <FoodCard
                    item={item}
                    copyFoodById={copyFood}
                    addImpression={addImpression}
                    removeImpression={removeImpression}
                    impressionRequestInProgress={impressionsForFoodsInProgress.some(
                      id => `${id}` === `${item.id}`
                    )}
                    copying={copyingFoods.some(
                      fId => `${fId}` === `${item.id}`
                    )}
                  />
                </GridItem>
              ))}
            </GridContainer>
          )}
        </>
      ) : (
        <>
          {lastSearchFilter !== "" && (
            <>
              <Typography className={classes.itemsGroupLabel}>
                {t("public_foods")}
              </Typography>
              <Typography className={classes.itemsGroupLabel}>
                {t("no_public_foods_found")}
              </Typography>
            </>
          )}
        </>
      )}
    </>
  );
};

export default connector(PublicFoods);
