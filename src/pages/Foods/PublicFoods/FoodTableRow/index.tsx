import React, { FC } from "react";
import {
  TableRow,
  TableCell,
  IconButton,
  Tooltip,
  Icon,
  makeStyles
} from "@material-ui/core";
import { Food } from "../../../../utils/types";
import { useTranslation } from "react-i18next";
import { uiNumber } from "../../../../utils/uiUtils";
import { Impression } from "../../../../utils/enums";

const useStyles = makeStyles(() => ({
  actionsCell: {
    minWidth: 176
  }
}));

type FoodTableRowProps = {
  item: Food;
  copying: boolean;
  impressionRequestInProgress: boolean;
  copyFoodById(foodId: Food["id"]): any;
  removeImpression(foodId: Food["id"]): any;
  addImpression(impression: Impression, foodId: Food["id"]): any;
};

const FoodTableRow: FC<FoodTableRowProps> = ({
  item,
  copying,
  impressionRequestInProgress,
  copyFoodById,
  removeImpression,
  addImpression
}) => {
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <TableRow>
      <TableCell
        className={classes.actionsCell}
        style={{
          paddingTop: 0,
          paddingBottom: 0
        }}
      >
        <Tooltip
          title={
            item.my_impression === Impression.LIKE
              ? t("liked_btn_hover_tooltip")
              : t("like_btn_hover_tooltip")
          }
        >
          <IconButton
            disabled={!!item.loadingImpressions || impressionRequestInProgress}
            onClick={() => {
              if (item.my_impression === Impression.LIKE) {
                removeImpression(Number(item.id));
              } else {
                addImpression(Impression.LIKE, Number(item.id));
              }
            }}
            aria-label={t("like")}
          >
            <Icon>thumb_up</Icon>
          </IconButton>
        </Tooltip>

        <Tooltip
          title={
            item.my_impression === Impression.DISLIKE
              ? t("disliked_btn_hover_tooltip")
              : t("dislike_btn_hover_tooltip")
          }
        >
          <IconButton
            disabled={!!item.loadingImpressions || impressionRequestInProgress}
            onClick={() => {
              if (item.my_impression === Impression.DISLIKE) {
                removeImpression(Number(item.id));
              } else {
                addImpression(Impression.DISLIKE, Number(item.id));
              }
            }}
            aria-label={t("dislike")}
          >
            <Icon>thumb_down</Icon>
          </IconButton>
        </Tooltip>

        <Tooltip title={t("copy_to_my_foods")}>
          <IconButton
            disabled={copying}
            onClick={() => {
              copyFoodById(Number(item.id));
            }}
          >
            <Icon>file_copy</Icon>
          </IconButton>
        </Tooltip>
      </TableCell>

      <TableCell>{item.likes}</TableCell>
      <TableCell>{item.dislikes}</TableCell>

      <TableCell>{item.name}</TableCell>
      <TableCell>{item.about}</TableCell>

      <TableCell>
        {t(`measurements_instrumental_case_lc.${item.measurement}`)}
      </TableCell>

      <TableCell>{uiNumber(item.kcal)}</TableCell>

      <TableCell>{uiNumber(item.prots)}</TableCell>
      <TableCell>{uiNumber(item.fats)}</TableCell>
      <TableCell>{uiNumber(item.carbs)}</TableCell>
    </TableRow>
  );
};

export default FoodTableRow;
