import React, { FC } from "react";
import { useTranslation } from "react-i18next";
import { Food } from "../../../../utils/types";
import {
  Card,
  CardContent,
  Typography,
  CardActions,
  Tooltip,
  IconButton,
  Icon,
  Button
} from "@material-ui/core";
import { uiNumber } from "../../../../utils/uiUtils";
import { Impression } from "../../../../utils/enums";

type FoodCardProps = {
  item: Food;
  copying: boolean;
  impressionRequestInProgress: boolean;
  copyFoodById(foodId: Food["id"]): any;
  removeImpression(foodId: Food["id"]): any;
  addImpression(impression: Impression, foodId: Food["id"]): any;
};

const FoodCard: FC<FoodCardProps> = ({
  item,
  copying,
  impressionRequestInProgress,
  copyFoodById,
  removeImpression,
  addImpression
}) => {
  const { t } = useTranslation();
  return (
    <Card>
      <CardContent>
        <Typography>{item.name}</Typography>
        <Typography>
          {item.about}{" "}
          {(item.kcal || item.prots || item.fats || item.carbs) &&
            item.measurement &&
            `${t("nutritional_value")} ${t("in")} ${t(
              `measurements_instrumental_case_lc.${item.measurement}`
            )}:`}
          {item.kcal && ` ${t("kcal")}: ${uiNumber(item.kcal)} `}
          {item.prots && ` ${t("prots")}: ${uiNumber(item.prots)} `}
          {item.fats && ` ${t("fats")}: ${uiNumber(item.fats)} `}
          {item.carbs && ` ${t("carbs")}: ${uiNumber(item.carbs)} `}
        </Typography>
      </CardContent>
      <CardActions>
        <Tooltip
          title={
            item.my_impression === Impression.LIKE
              ? t("liked_btn_hover_tooltip")
              : t("like_btn_hover_tooltip")
          }
        >
          <IconButton
            disabled={!!item.loadingImpressions || impressionRequestInProgress}
            onClick={() => {
              if (item.my_impression === Impression.LIKE) {
                removeImpression(Number(item.id));
              } else {
                addImpression(Impression.LIKE, Number(item.id));
              }
            }}
            aria-label={t("like")}
          >
            <Icon>thumb_up</Icon>
          </IconButton>
        </Tooltip>

        {Number(item.likes) > 0 && <Typography>{item.likes}</Typography>}

        <Tooltip
          title={
            item.my_impression === Impression.DISLIKE
              ? t("disliked_btn_hover_tooltip")
              : t("dislike_btn_hover_tooltip")
          }
        >
          <IconButton
            disabled={!!item.loadingImpressions || impressionRequestInProgress}
            onClick={() => {
              if (item.my_impression === Impression.DISLIKE) {
                removeImpression(Number(item.id));
              } else {
                addImpression(Impression.DISLIKE, Number(item.id));
              }
            }}
            aria-label={t("dislike")}
          >
            <Icon>thumb_down</Icon>
          </IconButton>
        </Tooltip>

        {Number(item.dislikes) > 0 && <Typography>{item.dislikes}</Typography>}

        <Button
          disabled={copying}
          onClick={() => {
            copyFoodById(Number(item.id));
          }}
        >
          {t("copy_to_my_foods")}
        </Button>
      </CardActions>
    </Card>
  );
};

export default FoodCard;
