import React, { FC } from "react";
import { Food } from "../../../utils/types";
import LoadingSpinner from "../../../components/LoadingSpinner";
import { WithTheme, Typography, makeStyles, Theme, withTheme } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import GridContainer from "../../../components/GridContainer";
import GridItem from "../../../components/GridItem";
import FoodListItem from "../FoodListItem";

const useStyles = makeStyles((theme: Theme) => ({
  itemsGroupLabel: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(2)
  }
}));

type OwnProps = {
  items: Food[];
  isLoading: boolean;
  filter: string;
};

type MyFoodsProps = WithTheme & OwnProps;

const MyFoods: FC<MyFoodsProps> = ({ theme, items, filter, isLoading }) => {
  const { t } = useTranslation();
  const classes = useStyles();

  if (isLoading) {
    return <LoadingSpinner />;
  }

  const filteredFoods = items.filter((food: Food) =>
    filter ? food.name.toUpperCase().includes(filter.toUpperCase()) : true
  );

  return (
    <>
      {items.length > 0 ? (
        <>
          {filteredFoods.length > 0 && (
            <>
              <Typography className={classes.itemsGroupLabel}>
                {t("my_foods")}
              </Typography>

              <GridContainer>
                {filteredFoods.map((item: Food) => (
                  <GridItem key={item.id}>
                    <FoodListItem item={item} />
                  </GridItem>
                ))}
              </GridContainer>
            </>
          )}
        </>
      ) : (
        <>
          <Typography
            style={{
              paddingTop: theme.spacing(2)
            }}
            gutterBottom
            color="inherit"
          >
            {t("no_foods_found")}
          </Typography>
          <Typography color="inherit">
            {t("search_or_add_some_foods")}
          </Typography>
        </>
      )}
    </>
  );
};

export default withTheme(MyFoods);
