import React, { FC } from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import queryString from "query-string";
import paths from "../../../paths/paths";
import { uiNumber } from "../../../utils/uiUtils";
import { Food } from "../../../utils/types";
import {
  CardContent,
  CardActions,
  Card,
  Typography,
  Button,
  withTheme,
  WithTheme
} from "@material-ui/core";

interface FoodListItemProps extends WithTheme {
  item: Food;
}

const FoodListItem: FC<FoodListItemProps> = ({ item, theme }) => {
  const { t } = useTranslation();

  const totalIsNotZero = !!(
    Number(item.protsPercent) +
    Number(item.fatsPercent) +
    Number(item.carbsPercent)
  );

  return (
    <Card>
      <CardContent style={{ padding: theme.spacing(2), paddingBottom: 0 }}>
        <Typography variant="h6">{item.name}</Typography>
        <Typography>
          {t("public")}:{" "}
          {item.public ? (
            <span role="img" aria-label={t("yes")}>
              ✔️
            </span>
          ) : (
            <span role="img" aria-label={t("no")}>
              ❌
            </span>
          )}
        </Typography>
        <Typography>
          {t("measured_by")}: {t(`measurements.${item.measurement}`)}
        </Typography>
        <Typography>
          {t("kcal")}: {uiNumber(item.kcal)}
        </Typography>
        <Typography>
          {t("prots")}: {uiNumber(item.prots)}
          {t("grams_acronym")}
          {totalIsNotZero && ` (${uiNumber(Number(item.protsPercent))}%)`}
        </Typography>
        <Typography>
          {t("fats")}: {uiNumber(item.fats)}
          {t("grams_acronym")}
          {totalIsNotZero && ` (${uiNumber(Number(item.fatsPercent))}%)`}
        </Typography>
        <Typography>
          {t("carbs")}: {uiNumber(item.carbs)}
          {t("grams_acronym")}
          {totalIsNotZero && ` (${uiNumber(Number(item.carbsPercent))}%)`}
        </Typography>
      </CardContent>

      <CardActions>
        <Button
          component={Link}
          to={`${paths.AddMeal.toPath()}?${queryString.stringify({
            preselected_food_id: item.id
          })}`}
        >
          {t("create_meal")}
        </Button>

        <Button
          component={Link}
          to={paths.MealsByFood.toPath({ foodId: item.id })}
        >
          {t("see_meals")}
        </Button>

        <Button
          component={Link}
          to={paths.EditFood.toPath({
            foodId: item.id
          })}
        >
          {t("edit")}
        </Button>
      </CardActions>
    </Card>
  );
};

export default withTheme(FoodListItem);
