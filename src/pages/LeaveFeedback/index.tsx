import React, { useState, FC, ChangeEvent } from "react";
import { useTranslation } from "react-i18next";
import { connect, ConnectedProps } from "react-redux";
import {
  Typography,
  TextField,
  withTheme,
  Button,
  WithTheme
} from "@material-ui/core";
import { setNotification } from "../../actions/commonActions";
import Page from "../../components/Page";
import axios from "../../utils/axios";
import endpoints from "../../utils/endpoints";
import { responseIsSuccessful } from "../../utils/variousUtils";
import { NotificationType } from "../../utils/enums";

const connector = connect(null, { setNotification });

type LeaveFeedbackProps = ConnectedProps<typeof connector> & WithTheme;

const LeaveFeedback: FC<LeaveFeedbackProps> = ({ setNotification, theme }) => {
  const { t } = useTranslation();

  const [feedbackText, setFeedbackText] = useState<string>("");
  const [reqInProgress, setReqInProgress] = useState<boolean>(false);

  const postFeedback = async () => {
    try {
      if (reqInProgress) {
        return setNotification({
          type: NotificationType.ERROR,
          message: "this_request_is_already_in_progress"
        });
      }

      setReqInProgress(true);

      const response = await axios.post(endpoints.POST_FEEDBACK, {
        feedback: feedbackText
      });

      if (responseIsSuccessful(response)) {
        setFeedbackText("");
        setNotification({
          type: NotificationType.SUCCESS,
          message: "feedback_sent"
        });
      }
    } catch (e) {
      setNotification({
        type: NotificationType.ERROR,
        message: "failed"
      });
      console.error(e);
    } finally {
      setReqInProgress(false);
    }
  };

  return (
    <Page
      style={{
        padding: theme.spacing(3),
        color: theme.palette.text.primary
      }}
    >
      <form
        onSubmit={e => {
          e.preventDefault();
          postFeedback();
        }}
      >
        <fieldset disabled={reqInProgress}>
          <Typography gutterBottom>
            {t("feedback_form_explanation_p1")}
          </Typography>
          <Typography>{t("feedback_form_explanation_p2")}</Typography>
          <div style={{ paddingBottom: theme.spacing(2) }}>
            <TextField
              fullWidth
              multiline
              margin="normal"
              rows="3"
              autoFocus
              autoComplete="off"
              required
              disabled={reqInProgress}
              label={t("feedback_textarea_placeholder")}
              value={feedbackText}
              onChange={(
                e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
              ) => setFeedbackText(e.target.value)}
            />
          </div>
          <div>
            <Button
              disabled={reqInProgress}
              type="submit"
              variant="contained"
              color="primary"
            >
              {t("send_feedback")}
            </Button>
          </div>
        </fieldset>
      </form>
    </Page>
  );
};

export default withTheme(connector(LeaveFeedback));
