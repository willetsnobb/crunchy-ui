import constants from "../utils/constants";
import { Action, CommonReducerState } from "../utils/types";
import { ThemeVariant, LSKey } from "../utils/enums";
import { getLSKey } from "../utils/localstorage";

const INITIAL_STATE = {
  header: null,
  drawerOpen: false,
  useTables: false,
  persistDrawer: false,
  notification: null,
  theme: getLSKey(LSKey.THEME_VARIANT, ThemeVariant.LIGHT),
  path: "/"
};

export default (
  state: CommonReducerState = INITIAL_STATE,
  action: Action
): CommonReducerState => {
  switch (action.type) {
    case constants.SET_USE_TABLES:
      return { ...state, useTables: action.payload };

    case constants.SET_PATH:
      return { ...state, path: action.payload };

    case constants.SET_THEME:
      return { ...state, theme: action.payload };

    case constants.SET_NOTIFICATION:
      return { ...state, notification: action.payload };

    case constants.SET_HEADER:
      return { ...state, header: action.payload };

    case constants.SET_DRAWER_OPEN:
      return { ...state, drawerOpen: action.payload };

    case constants.SET_PERSIST_DRAWER:
      return { ...state, persistDrawer: action.payload };

    case constants.DROP_STATE_ON_LOGOUT:
      return {
        ...INITIAL_STATE,
        theme: state.theme,
        persistDrawer: state.persistDrawer
      };

    default:
      return state;
  }
};
