import constants from "../utils/constants";
import { Action, FoodsReducerState, Food } from "../utils/types";

const INITIAL_STATE: FoodsReducerState = {
  list: [],
  creating: false,
  updating: false,
  loadingList: false,
  copying: []
};

export default (
  state: FoodsReducerState = INITIAL_STATE,
  action: Action
): FoodsReducerState => {
  switch (action.type) {
    case constants.FOOD_COPY_REQUEST:
      return {
        ...state,
        copying: [...state.copying, action.payload]
      };
    case constants.FOOD_COPY_FAIL:
      return {
        ...state,
        copying: state.copying.filter(
          (foodId: Food["id"]): boolean => foodId !== action.payload
        )
      };
    case constants.FOOD_COPY_SUCCESS:
      return {
        ...state,
        list: [action.payload.food, ...state.list],
        copying: state.copying.filter(
          (foodId: Food["id"]): boolean => foodId !== action.payload.foodId
        )
      };

    case constants.FOOD_GET_ALL_REQUEST:
      return { ...state, loadingList: true, list: [] };
    case constants.FOOD_GET_ALL_FAIL:
      return { ...state, loadingList: false, list: [] };
    case constants.FOOD_GET_ALL_SUCCESS:
      return { ...state, loadingList: false, list: action.payload };

    case constants.FOOD_UPDATE_REQUEST:
      return { ...state, updating: true };
    case constants.FOOD_UPDATE_FAIL:
      return { ...state, updating: false };
    case constants.FOOD_UPDATE_SUCCESS:
      return {
        ...state,
        updating: false,
        list: state.list.map(
          (f: Food): Food => (f.id === action.payload.id ? action.payload : f)
        )
      };

    case constants.FOOD_CREATE_REQUEST:
      return { ...state, creating: true };
    case constants.FOOD_CREATE_FAIL:
      return { ...state, creating: false };
    case constants.FOOD_CREATE_SUCCESS:
      return {
        ...state,
        creating: false,
        list: [action.payload, ...state.list]
      };

    case constants.DROP_STATE_ON_LOGOUT: {
      return { ...INITIAL_STATE };
    }

    default:
      return state;
  }
};
