import constants from "../utils/constants";
import { LoginReducerState, Action } from "../utils/types";

const INITIAL_STATE: LoginReducerState = {
  user: null,
  token: null,
  tokenExpiresAt: 0
};

export default (
  state: LoginReducerState = INITIAL_STATE,
  action: Action
): LoginReducerState => {
  switch (action.type) {
    case constants.SET_TOKEN_EXPIRES_AT:
      return { ...state, tokenExpiresAt: action.payload };

    case constants.SET_TOKEN:
      return { ...state, token: action.payload };

    case constants.SET_LOGIN_USER:
      return { ...state, user: action.payload };

    case constants.DROP_STATE_ON_LOGOUT: {
      return { ...INITIAL_STATE };
    }

    default:
      return state;
  }
};
