import constants from "../utils/constants";
import { RequestStatus, MealIntervalOption } from "../utils/enums";
import { Action, MealsReducerState, Meal } from "../utils/types";

const INITIAL_STATE = {
  interval: MealIntervalOption.THIS_MONTH,
  list: [],
  getRequestStatus: RequestStatus.NOT_REQUESTED,
  postRequestStatus: RequestStatus.NOT_REQUESTED,
  putRequestStatus: RequestStatus.NOT_REQUESTED,
  deleteRequestStatus: RequestStatus.NOT_REQUESTED
};

export default (
  state: MealsReducerState = INITIAL_STATE,
  action: Action
): MealsReducerState => {
  switch (action.type) {
    case constants.SET_MEAL_INTERVAL:
      return { ...state, interval: action.payload };

    case constants.SET_MEALS_LIST:
      return { ...state, list: action.payload };

    case constants.SET_MEALS_GET_REQUEST_STATUS:
      return { ...state, getRequestStatus: action.payload };

    case constants.SET_MEAL_POST_REQUEST_STATUS:
      return { ...state, postRequestStatus: action.payload };

    case constants.SET_MEAL_PUT_REQUEST_STATUS:
      return { ...state, putRequestStatus: action.payload };

    case constants.ADD_MEAL:
      return { ...state, list: [action.payload, ...state.list] };

    case constants.UPDATE_MEAL:
      return {
        ...state,
        list: state.list.map(
          (meal: Meal): Meal =>
            meal.id === action.payload.id
              ? { ...meal, ...action.payload }
              : meal
        )
      };

    case constants.MEAL_DELETE_REQUEST:
      return {
        ...state,
        deleteRequestStatus: RequestStatus.FETCHING,
        list: state.list.map(
          (meal: Meal): Meal =>
            meal.id === action.payload ? { ...meal, deleting: true } : meal
        )
      };
    case constants.MEAL_DELETE_FAIL:
      return {
        ...state,
        deleteRequestStatus: RequestStatus.FAIL,
        list: state.list.map(
          (meal: Meal): Meal =>
            meal.id === action.payload ? { ...meal, deleting: false } : meal
        )
      };
    case constants.MEAL_DELETE_SUCCESS:
      return {
        ...state,
        deleteRequestStatus: RequestStatus.SUCCESS,
        list: state.list.filter(
          (meal: Meal): boolean => meal.id !== action.payload
        )
      };

    case constants.DROP_STATE_ON_LOGOUT: {
      return { ...INITIAL_STATE };
    }

    default:
      return state;
  }
};
