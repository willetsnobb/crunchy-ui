import { combineReducers } from "redux";
import foods from "./foodsReducer";
import meals from "./mealsReducer";
import register from "./registerReducer";
import login from "./loginReducer";
import common from "./commonReducer";

const rootReducer = combineReducers({
  meals,
  foods,
  register,
  login,
  common
});

export default rootReducer;

export type ReduxState = ReturnType<typeof rootReducer>;
