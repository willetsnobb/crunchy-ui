import constants from "../utils/constants";
import { RequestStatus } from "../utils/enums";
import { Action, RegisterReducerState } from "../utils/types";

const INITIAL_STATE = {
  requestStatus: RequestStatus.NOT_REQUESTED
};

export default (
  state: RegisterReducerState = INITIAL_STATE,
  action: Action
): RegisterReducerState => {
  switch (action.type) {
    case constants.SET_REGISTER_REQUEST_STATUS:
      return { ...state, requestStatus: action.payload };

    case constants.DROP_STATE_ON_LOGOUT: {
      return { ...INITIAL_STATE };
    }

    default:
      return state;
  }
};
